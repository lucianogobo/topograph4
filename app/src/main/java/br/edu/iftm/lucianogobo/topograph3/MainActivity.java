package br.edu.iftm.lucianogobo.topograph3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import br.edu.iftm.lucianogobo.topograph3.activities.Area2;
import br.edu.iftm.lucianogobo.topograph3.activities.AreaActivity;
import br.edu.iftm.lucianogobo.topograph3.activities.InputInitialActivities;
import br.edu.iftm.lucianogobo.topograph3.activities.LevelCurveActivity;

public class MainActivity extends AppCompatActivity {

    Area2 area2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickNewInputData(View view){
        Intent newInputData = new Intent(this, InputInitialActivities.class);
        startActivity(newInputData);
    }

    public void onClickArea(View view){
        Intent area = new Intent(this, AreaActivity.class);
        startActivity(area);
    }

    public void onClickArea2(View view){
     //   area2 =new Area2(this);
     //   area2.setBackgroundColor(Color.WHITE);
     //   setContentView(area2);
        Intent area2 = new Intent(this, LevelCurveActivity.class);
        startActivity(area2);
    }

    public void onExit(View view){
        finish();
    }

}