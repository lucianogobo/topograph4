package br.edu.iftm.lucianogobo.topograph3.activities;

import static br.edu.iftm.lucianogobo.topograph3.activities.NewImputTopoActivity.contador;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import br.edu.iftm.lucianogobo.topograph3.R;
import br.edu.iftm.lucianogobo.topograph3.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph3.model.Topo;

public class TopoTableOne extends AppCompatActivity {

    public static final String RESULT_KEY="NewTopoTableActivity.RESULT_KEY";
    static int modifyVertice;
    private LinearLayout lnhTopoList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topo_table_one);
        this.lnhTopoList=findViewById(R.id.lnhTopoList);
        this.buildStoreList();
    }

    private void buildStoreList() {
        this.lnhTopoList.removeAllViews();
        int contador=0;
        for (int i=-1;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++) {
            contador=contador+1;
            View view = getLayoutInflater().inflate(R.layout.activity_row_table,this.lnhTopoList, false);
            TextView coluna1 = view.findViewById(R.id.textView21);
            TextView coluna2 = view.findViewById(R.id.textView20);
            TextView coluna3 = view.findViewById(R.id.textView19);
            TextView coluna4 = view.findViewById(R.id.textView18);
            TextView coluna5 = view.findViewById(R.id.textView17);
            TextView coluna6 = view.findViewById(R.id.textView16);
            TextView coluna7 = view.findViewById(R.id.textView13);
            TextView coluna8 = view.findViewById(R.id.textView14);
            TextView coluna9 = view.findViewById(R.id.textView15);
            TextView coluna10 = view.findViewById(R.id.textView22);
            TextView coluna11 = view.findViewById(R.id.textView23);
            if(i==-1){
                coluna1.setText("Point");
                coluna2.setText("Direction");
                coluna3.setText("Aft angle");
                coluna4.setText("Forward angle");
                coluna5.setText("Zenith");
                coluna6.setText("Botton thread");
                coluna7.setText("Medium thread");
                coluna8.setText("Top thread");
                coluna9.setText("Theod. height");
                coluna10.setText("Modify");
                coluna10.setText("Trash");
            }else{
                coluna1.setText(Integer.toString(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto()));
                coluna2.setText(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDirecao());
                coluna3.setText(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloRE());
                coluna4.setText(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloVANTE());
                coluna5.setText(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getZenite());
                coluna6.setText(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getFioInferior());
                coluna7.setText(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getFioMedio());
                coluna8.setText(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getFioSuperior());
                coluna9.setText(Double.toString(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAlturaAparelho()));
            }

            if(i>-1){
                coluna11.setTag(DAOTopoSingleton.getINSTANCE().getTopos().get(i));
                int finalI = i;
                coluna11.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int ponto;
                        ponto= finalI;
                        deleteStoreList(ponto);
                    }
                });

                coluna10.setTag(DAOTopoSingleton.getINSTANCE().getTopos().get(i));
                coluna10.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int ponto;
                        ponto= finalI;
                        modifyStoreList(ponto);
                    }
                });
            }
            this.lnhTopoList.addView(view);
        }
    }

    public void onClick2(View view){
        int ponto;
        int idtrash;
        TextView ttexto;
        String texto;
        idtrash=view.getId();
        ttexto=findViewById(idtrash);
        texto=ttexto.getText().toString();
        ponto=Integer.parseInt(texto);
        modifyStoreList(ponto);
    }

    private void modifyStoreList(int ponto){
                modifyVertice=ponto;
                Intent newModifyData = new Intent(this,TopoModifyActivity.class);
                startActivity(newModifyData);
                finish();
    }

    private void deleteStoreList(int ponto){
        DAOTopoSingleton.getINSTANCE().removeStore(DAOTopoSingleton.getINSTANCE().getTopos().get(ponto));
        contador=contador-1;
        buildStoreList();

    }

    public void onExit(View view){
        finish();
    }
}