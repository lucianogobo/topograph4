package br.edu.iftm.lucianogobo.topograph3.data;

import java.util.ArrayList;

import br.edu.iftm.lucianogobo.topograph3.model.Topo;
import br.edu.iftm.lucianogobo.topograph3.model.Triangulo;

public class DAOTrianguloSingleton {

    private static DAOTrianguloSingleton INSTANCE;

    //-----recurso globalmente acessado
    private ArrayList<Triangulo> triangulos;

    private DAOTrianguloSingleton(){
        this.triangulos =new ArrayList<>();
    }

    //Garantir que INSTANCE é modificado apenas 1 vez
    public static DAOTrianguloSingleton getINSTANCE(){
        if(INSTANCE==null){
            INSTANCE =new DAOTrianguloSingleton();
        }
        return INSTANCE;
    }

    //Definir regras aos recursos globais

    public ArrayList<Triangulo> getTriangulo(){
        return this.triangulos;
    }

    public void addTriangulo(Triangulo triangulo){
        this.triangulos.add(triangulo);
    }

    public void modTriangulo(int i,Triangulo triangulo){
        this.triangulos.add(i,triangulo);
    }

    public void removeTriangulo(Triangulo triangulo) {this.triangulos.remove(triangulo);}

    public int sendSize(){return this.triangulos.size();}

}
