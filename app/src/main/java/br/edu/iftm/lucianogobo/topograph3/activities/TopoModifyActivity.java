package br.edu.iftm.lucianogobo.topograph3.activities;

import static br.edu.iftm.lucianogobo.topograph3.activities.TopoTableOne.modifyVertice;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import br.edu.iftm.lucianogobo.topograph3.R;
import br.edu.iftm.lucianogobo.topograph3.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph3.model.Topo;

public class TopoModifyActivity extends AppCompatActivity {


    public static final String RESULT_KEY="NewTopoTableActivity.RESULT_KEY";
    private TextView Mod1;
    private TextView Mod2;
    private TextView Mod3;
    private TextView Mod4;
    private TextView Mod5;
    private TextView Mod6;
    private TextView Mod7;
    private TextView Mod8;
    private TextView Mod9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topo_modify);
        this.Mod1 = findViewById(R.id.textView92);
        this.Mod2 = findViewById(R.id.editTextTextPersonName9);
        this.Mod3 = findViewById(R.id.editTextTextPersonName10);
        this.Mod4 = findViewById(R.id.editTextTextPersonName11);
        this.Mod5 = findViewById(R.id.editTextTextPersonName12);
        this.Mod6 = findViewById(R.id.editTextTextPersonName13);
        this.Mod7 = findViewById(R.id.editTextTextPersonName14);
        this.Mod8 = findViewById(R.id.editTextTextPersonName15);
        this.Mod9 = findViewById(R.id.editTextNumberDecimal2);
        buildStoreList();
    }

    private void buildStoreList() {
        System.out.println("Passou no buildStoreList");
        int verticeModificado=modifyVertice;
        Topo topo=DAOTopoSingleton.getINSTANCE().getTopos().get(verticeModificado);
        System.out.println(topo.getPonto());
        System.out.println(topo.getDirecao());
        System.out.println(topo.getAnguloRE());
        System.out.println(topo.getAnguloVANTE());
        System.out.println(topo.getZenite());
        System.out.println(topo.getFioInferior());
        System.out.println(topo.getFioMedio());
        System.out.println(topo.getFioSuperior());
        System.out.println(topo.getAlturaAparelho());
        Mod1.setText(Integer.toString(topo.getPonto()));
        Mod2.setText(topo.getDirecao());
        Mod3.setText(topo.getAnguloRE());
        Mod4.setText(topo.getAnguloVANTE());
        Mod5.setText(topo.getZenite());
        Mod6.setText(topo.getFioInferior());
        Mod7.setText(topo.getFioMedio());
        Mod8.setText(topo.getFioSuperior());
        Mod9.setText(Double.toString(topo.getAlturaAparelho()));
    }

    public void modifyText(View view){
        int verticeModificado=modifyVertice;
        Topo topo=DAOTopoSingleton.getINSTANCE().getTopos().get(verticeModificado);
        topo.setPonto(Integer.parseInt(Mod1.getText().toString()));
        topo.setDirecao(Mod2.getText().toString());
        topo.setAnguloRE(Mod3.getText().toString());
        topo.setAnguloVANTE(Mod4.getText().toString());
        topo.setZenite(Mod5.getText().toString());
        topo.setFioInferior(Mod6.getText().toString());
        topo.setFioMedio(Mod7.getText().toString());
        topo.setFioSuperior(Mod8.getText().toString());
        topo.setAlturaAparelho(Double.parseDouble(Mod9.getText().toString()));
        AreaActivity areaActivity=new AreaActivity();
        double angularReD=areaActivity.anguloFormatoDecimal(topo.getAnguloRE());
        double angularVanteD=areaActivity.anguloFormatoDecimal(topo.getAnguloVANTE());
        double angulohorizontalD=angularVanteD-angularReD;
        String anguloHorizontal=areaActivity.anguloFormatoSexadecimal(angulohorizontalD);
        topo.setAnguloHorizontal(anguloHorizontal);
        DAOTopoSingleton.getINSTANCE().modStore(topo);
        Intent newTableData = new Intent(this,TopoTableOne.class);
        startActivity(newTableData);
        finish();
    }
}