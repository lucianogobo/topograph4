package br.edu.iftm.lucianogobo.topograph3.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.DisplayCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import br.edu.iftm.lucianogobo.topograph3.R;
import br.edu.iftm.lucianogobo.topograph3.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph3.data.DAOTrianguloSingleton;
import br.edu.iftm.lucianogobo.topograph3.model.Coordenadas;
import br.edu.iftm.lucianogobo.topograph3.model.Topo;
import br.edu.iftm.lucianogobo.topograph3.model.Triangulo;
import br.edu.iftm.lucianogobo.topograph3.service.Service;

public class LevelCurveActivity extends AppCompatActivity {

    private ImageView imageView;
    private Button button;
    private ArrayList<Coordenadas> coordiso= new ArrayList<Coordenadas>();
    final String ARQUIVO = "arquivo.lsp";
    private StringBuilder builder = new StringBuilder("");
    private StringBuilder builder2 = new StringBuilder("");




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_curve);
        this.imageView=findViewById(R.id.imageView2);
        this.button=findViewById(R.id.button12);
        contorno();
    }

    // Auxiliares

    public double maiorCota1(){
        double maiorCota=0.0;
        for (Triangulo triangulo : DAOTrianguloSingleton.getINSTANCE().getTriangulo()) {
            if (triangulo.getVertice1().getZ() > maiorCota) {
                maiorCota = triangulo.getVertice1().getZ();
            }
            if (triangulo.getVertice2().getZ() > maiorCota) {
                maiorCota = triangulo.getVertice2().getZ();
            }
            if (triangulo.getVertice3().getZ() > maiorCota) {
                maiorCota = triangulo.getVertice3().getZ();
            }

        }
        return maiorCota;
    }

    // Fim das Auxiliares



    // Curvas de nível

    public void curvas(Bitmap bitmap,double xMedia, double yMedia,double fatorescala,
                       float xPartida,float yPartida){

        double intervaloCurvas = 1.0;
    /*    Bitmap bitmap = Bitmap.createBitmap(391,630,Bitmap.Config.ARGB_8888);
        bitmap=bitmap.copy(bitmap.getConfig(), true); */
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4f);
        paint.setTextSize(14f);
        paint.setColor(Color.DKGRAY);
        paint.setStrokeWidth(2f);
        double iD = 0.0;
        double maiorCota=maiorCota1();
        while ( iD < maiorCota) {
            for (Triangulo triangulo : DAOTrianguloSingleton.getINSTANCE().getTriangulo()) {
                Coordenadas noInicial= new Coordenadas();
                Coordenadas noFinal= new Coordenadas();
                if (iD > triangulo.getVerticeMenor().getZ()) {
                    Triangulo triPontos = new Triangulo();
                    if (iD > triangulo.getVerticeMeio().getZ()) {
                        noInicial = triPontos.pontoCurvaNivel(triangulo, 2, iD);
                    } else {
                        noInicial = triPontos.pontoCurvaNivel(triangulo, 1, iD);
                    }
                    noFinal = triPontos.pontoCurvaNivel(triangulo, 3, iD);
                    float xInicial = (float)xPartida + ((float) noInicial.getX() - (float) xMedia) * (float) fatorescala;
                    float yInicial = (float)yPartida - ((float) noInicial.getY() - (float) yMedia) * (float) fatorescala;
                    float xFinal=(float)xPartida + ((float) noFinal.getX() - (float) xMedia) * (float) fatorescala;
                    float yFinal= (float)yPartida - ((float) noFinal.getY() - (float) yMedia) * (float) fatorescala;
                    double pt1X=noInicial.getX();
                    double pt1Y=noInicial.getY();
                    double pt2X=noFinal.getX();
                    double pt2Y=noFinal.getY();
                    builder2.append("(setq pt1 '("+pt1X+" " +pt1Y+") pt2 '("+pt2X+" "+pt2Y+"))\n");
                    builder2.append("(command \"._line\" pt1 pt2 \"\")");
                    canvas.drawLine(xInicial, yInicial, xFinal, yFinal,paint);
                }
            }
            iD = iD + intervaloCurvas;
        }
        imageView.setImageBitmap(bitmap);
    }

    public void curvas2(ImageView imvDeleteArea,Bitmap bitmap,double xMedia, double yMedia,double fatorescala,
                        float xPartida,float yPartida){

        double intervaloCurvas = 5.0;
    /*    Bitmap bitmap = Bitmap.createBitmap(391,630,Bitmap.Config.ARGB_8888);
        bitmap=bitmap.copy(bitmap.getConfig(), true); */
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4f);
        paint.setTextSize(14f);
        paint.setColor(Color.DKGRAY);
        paint.setStrokeWidth(2f);
        int contaArea=0;
        for (Triangulo triangulo : DAOTrianguloSingleton.getINSTANCE().getTriangulo()) {
            contaArea++;
            float maiorX= 0.0F;
            float menorX= 1000000.0F;
            float maiorY= 0.0F;
            float menorY= 1000000.0F;
            Coordenadas noInicial1= new Coordenadas();
            Coordenadas noFinal1= new Coordenadas();
            noInicial1.setX(triangulo.getVertice1().getX());
            noInicial1.setY(triangulo.getVertice1().getY());
            noFinal1.setX(triangulo.getVertice2().getX());
            noFinal1.setY(triangulo.getVertice2().getY());
            float xInicial = (float)xPartida + ((float) noInicial1.getX() - (float) xMedia) * (float) fatorescala;
            float yInicial = (float)yPartida - ((float) noInicial1.getY() - (float) yMedia) * (float) fatorescala;
            float xFinal=(float)xPartida + ((float) noFinal1.getX() - (float) xMedia) * (float) fatorescala;
            float yFinal= (float)yPartida - ((float) noFinal1.getY() - (float) yMedia) * (float) fatorescala;
            canvas.drawLine(xInicial, yInicial, xFinal, yFinal,paint);
            if(xInicial>maiorX){
                maiorX=xInicial;
            }
            if(xInicial<menorX){
                menorX=xInicial;
            }
            if(yInicial>maiorY){
                maiorY=yInicial;
            }
            if(yInicial<menorY){
                menorY=yInicial;
            }

            Coordenadas noInicial2= new Coordenadas();
            Coordenadas noFinal2= new Coordenadas();
            noInicial2.setX(triangulo.getVertice2().getX());
            noInicial2.setY(triangulo.getVertice2().getY());
            noFinal2.setX(triangulo.getVertice3().getX());
            noFinal2.setY(triangulo.getVertice3().getY());

            xInicial = (float)xPartida + ((float) noInicial2.getX() - (float) xMedia) * (float) fatorescala;
            yInicial = (float)yPartida - ((float) noInicial2.getY() - (float) yMedia) * (float) fatorescala;
            xFinal=(float)xPartida + ((float) noFinal2.getX() - (float) xMedia) * (float) fatorescala;
            yFinal= (float)yPartida - ((float) noFinal2.getY() - (float) yMedia) * (float) fatorescala;
            canvas.drawLine(xInicial, yInicial, xFinal, yFinal,paint);
            if(xInicial>maiorX){
                maiorX=xInicial;
            }
            if(xInicial<menorX){
                menorX=xInicial;
            }
            if(yInicial>maiorY){
                maiorY=yInicial;
            }
            if(yInicial<menorY){
                menorY=yInicial;
            }
            Coordenadas noInicial3= new Coordenadas();
            Coordenadas noFinal3= new Coordenadas();
            noInicial3.setX(triangulo.getVertice3().getX());
            noInicial3.setY(triangulo.getVertice3().getY());
            noFinal3.setX(triangulo.getVertice1().getX());
            noFinal3.setY(triangulo.getVertice1().getY());

            xInicial = (float)xPartida + ((float) noInicial3.getX() - (float) xMedia) * (float) fatorescala;
            yInicial = (float)yPartida - ((float) noInicial3.getY() - (float) yMedia) * (float) fatorescala;
            xFinal=(float)xPartida + ((float) noFinal3.getX() - (float) xMedia) * (float) fatorescala;
            yFinal= (float)yPartida - ((float) noFinal3.getY() - (float) yMedia) * (float) fatorescala;
            canvas.drawLine(xInicial, yInicial, xFinal, yFinal,paint);

            if(xInicial>maiorX){
                maiorX=xInicial;
            }
            if(xInicial<menorX){
                menorX=xInicial;
            }
            if(yInicial>maiorY){
                maiorY=yInicial;
            }
            if(yInicial<menorY){
                menorY=yInicial;
            }

            float xMT= (maiorX+menorX)/2;
            float yMT= (maiorY+menorY)/2;
            String areaTexto=String.valueOf(contaArea);
            canvas.drawText(areaTexto, xMT,yMT,paint);
        }
        imvDeleteArea.setImageBitmap(bitmap);
    }

    // Fim das curvas de nível

    // Contornos das areas

    public void contorno(){
        Bitmap bitmap = Bitmap.createBitmap(391,630,Bitmap.Config.ARGB_8888);
        bitmap=bitmap.copy(bitmap.getConfig(), true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4f);
        paint.setTextSize(14f);
        Service service=new Service();
        service.coordenadasP();
        ArrayList<Coordenadas> coord = new ArrayList<Coordenadas>();
        for(Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()){
            Coordenadas coordenadas = new Coordenadas();
            coordenadas.setX(topo.getDistanciaX());
            coordenadas.setY(topo.getDistanciaY());
            coordenadas.setZ(topo.getDistanciaZ());
            coord.add(coordenadas);
        }

        double fatorescala;
        double comprimentoPadrao;
        double xmax;
        double ymax;
        double xmin;
        double ymin;
        double xMedio;
        double yMedio;
        double xMedia;
        double yMedia;
        double comprimentoX;
        double comprimentoY;
        comprimentoX=canvas.getWidth();
        comprimentoY=canvas.getHeight();
        if(comprimentoX>=comprimentoY){
            comprimentoPadrao=comprimentoY;
        }else{
            comprimentoPadrao=comprimentoX;
        }
        xmax=0.0;
        ymax=0.0;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()>xmax){
                xmax=coord.get(i).getX();
            }
            if(coord.get(i).getY()>ymax){
                ymax=coord.get(i).getY();
            }
        }
        xmin=xmax;
        ymin=ymax;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()<xmin){
                xmin=coord.get(i).getX();
            }
            if(coord.get(i).getY()<ymin){
                ymin=coord.get(i).getY();
            }
        }
        xMedio=(xmax-xmin)/2.0;
        yMedio=(ymax-ymin)/2.0;
        xMedia=(xmax+xmin)/2.0;
        yMedia=(ymax+ymin)/2.0;
        if(xMedio>=yMedio){
            fatorescala=(comprimentoPadrao/2.0)/xMedio;
        }else{
            fatorescala=(comprimentoPadrao/2.0)/yMedio;
        }
        float xPartida=(float)comprimentoX/2;
        float yPartida=(float)comprimentoY/2;

        for(int i=1;i<coord.size();i++){
            Coordenadas coordeiso = new Coordenadas();
            float xInicial=xPartida+((float)coord.get(i-1).getX()-(float)xMedia)*(float)fatorescala;
            float yInicial=yPartida-((float)coord.get(i-1).getY()-(float)yMedia)*(float)fatorescala;
            float xFinal;
            float yFinal;
            if(i==coord.size()){
                xFinal=xPartida+((float)coord.get(0).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=yPartida-((float)coord.get(0).getY()-(float)yMedia)*(float)fatorescala;
            }else{
                xFinal=(float)xPartida+((float)coord.get(i).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=(float)yPartida-((float)coord.get(i).getY()-(float)yMedia)*(float)fatorescala;
                canvas.drawLine(xInicial,yInicial,xFinal,yFinal,paint);
                if(i<coord.size()){
                    String pontoi=String.valueOf(i);
                    String ponto1x = String.format("%.3f",coord.get(i-1).getX());
                    String ponto1y = String.format("%.3f",coord.get(i-1).getY());
                    String ponto =pontoi+ "("+ponto1x+"," +ponto1y+")";

                    //           canvas.drawText(ponto, xInicial+5,yInicial-20,paint);
                //    ponto ="("+Double.toString(coord.get(i-1).getX())+","
                //            +Double.toString(coord.get(i-1).getY())+")";
                    canvas.drawText(ponto, xInicial+5,yInicial-5,paint);
                }
            }
            coordeiso.setX(xInicial);
            coordeiso.setY(yInicial);
            coordeiso.setZ(coord.get(i).getZ());
            coordiso.add(coordeiso);
        }
        curvas(bitmap,xMedia,yMedia,fatorescala,xPartida,yPartida);
        //imageView.setImageBitmap(bitmap);

    }

    public void contorno2(ImageView imvDeleteArea){
        Bitmap bitmap = Bitmap.createBitmap(391,630,Bitmap.Config.ARGB_8888);
        bitmap=bitmap.copy(bitmap.getConfig(), true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4f);
        paint.setTextSize(14f);
        Service service=new Service();
        service.coordenadasP();
        ArrayList<Coordenadas> coord = new ArrayList<Coordenadas>();
        for(Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()){
            Coordenadas coordenadas = new Coordenadas();
            coordenadas.setX(topo.getDistanciaX());
            coordenadas.setY(topo.getDistanciaY());
            coordenadas.setZ(topo.getDistanciaZ());
            coord.add(coordenadas);
        }
        double fatorescala;
        double comprimentoPadrao;
        double xmax;
        double ymax;
        double xmin;
        double ymin;
        double xMedio;
        double yMedio;
        double xMedia;
        double yMedia;
        double comprimentoX;
        double comprimentoY;
        comprimentoX=canvas.getWidth();
        comprimentoY=canvas.getHeight();
        if(comprimentoX>=comprimentoY){
            comprimentoPadrao=comprimentoY;
        }else{
            comprimentoPadrao=comprimentoX;
        }
        xmax=0.0;
        ymax=0.0;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()>xmax){
                xmax=coord.get(i).getX();
            }
            if(coord.get(i).getY()>ymax){
                ymax=coord.get(i).getY();
            }
        }
        xmin=xmax;
        ymin=ymax;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()<xmin){
                xmin=coord.get(i).getX();
            }
            if(coord.get(i).getY()<ymin){
                ymin=coord.get(i).getY();
            }
        }
        xMedio=(xmax-xmin)/2.0;
        yMedio=(ymax-ymin)/2.0;
        xMedia=(xmax+xmin)/2.0;
        yMedia=(ymax+ymin)/2.0;
        if(xMedio>=yMedio){
            fatorescala=(comprimentoPadrao/2.0)/xMedio;
        }else{
            fatorescala=(comprimentoPadrao/2.0)/yMedio;
        }
        float xPartida=(float)comprimentoX/2;
        float yPartida=(float)comprimentoY/2;

        for(int i=1;i<coord.size();i++){
            Coordenadas coordeiso = new Coordenadas();
            float xInicial=xPartida+((float)coord.get(i-1).getX()-(float)xMedia)*(float)fatorescala;
            float yInicial=yPartida-((float)coord.get(i-1).getY()-(float)yMedia)*(float)fatorescala;
            float xFinal;
            float yFinal;
            if(i==coord.size()){
                xFinal=xPartida+((float)coord.get(0).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=yPartida-((float)coord.get(0).getY()-(float)yMedia)*(float)fatorescala;
            }else{
                xFinal=(float)xPartida+((float)coord.get(i).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=(float)yPartida-((float)coord.get(i).getY()-(float)yMedia)*(float)fatorescala;
                canvas.drawLine(xInicial,yInicial,xFinal,yFinal,paint);
                if(i<coord.size()){
                    String ponto=Integer.toString(i);
                    //           canvas.drawText(ponto, xInicial+5,yInicial-20,paint);
                    ponto ="("+Double.toString(coord.get(i-1).getX())+","
                            +Double.toString(coord.get(i-1).getY())+")";
                    canvas.drawText(ponto, xInicial+5,yInicial-5,paint);
                }
            }
            coordeiso.setX(xInicial);
            coordeiso.setY(yInicial);
            coordeiso.setZ(coord.get(i).getZ());
            coordiso.add(coordeiso);
        }
        curvas2(imvDeleteArea,bitmap,xMedia,yMedia,fatorescala,xPartida,yPartida);


    }

    // Fim do Contorno das Areas

    // Inserção e remoção

    public void remocaoArea(int intremove ){
        if(validaAreaDelete(intremove)){
            DAOTrianguloSingleton.getINSTANCE().removeTriangulo(
                    DAOTrianguloSingleton.getINSTANCE().getTriangulo().get(intremove-1));
                    contorno();
        }else{

            View view2 = null;
            ShowDeleteVertices(view2);
        }
    }

    public void instanciaTriangulo(int vertice1,int vertice2,int vertice3){
        if(validaVertice(vertice1,vertice2,vertice3)){
            double maiorCota = 0.0;
            double menorCota = 10000.0;
            Coordenadas coord1=new Coordenadas();
            Coordenadas coord2=new Coordenadas();
            Coordenadas coord3=new Coordenadas();
            for (Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()) {
                if(topo.getPonto()==vertice1){
                    coord1.setX(topo.getDistanciaX());
                    coord1.setY(topo.getDistanciaY());
                    coord1.setZ(topo.getDistanciaZ());
                }
                if(topo.getPonto()==vertice2){
                    coord2.setX(topo.getDistanciaX());
                    coord2.setY(topo.getDistanciaY());
                    coord2.setZ(topo.getDistanciaZ());
                }
                if(topo.getPonto()==vertice3){
                    coord3.setX(topo.getDistanciaX());
                    coord3.setY(topo.getDistanciaY());
                    coord3.setZ(topo.getDistanciaZ());
                }
            }

            Triangulo triangulo=new Triangulo(coord1,coord2,coord3,null,null,null);
            double tri1 = triangulo.getVertice1().getZ();
            double tri2 = triangulo.getVertice2().getZ();
            double tri3 = triangulo.getVertice3().getZ();
            if (tri1 > maiorCota) {
                maiorCota = tri1;
            }
            if (tri2 > maiorCota) {
                maiorCota = tri2;
            }
            if (tri3 > maiorCota) {
                maiorCota = tri3;
            }
            if (tri1 < menorCota) {
                menorCota = tri1;
            }
            if (tri2 < menorCota) {
                menorCota = tri2;
            }
            if (tri3 < menorCota) {
                menorCota = tri3;
            }
            if (tri1 == tri2 && tri2 == tri3) {
                triangulo.setVerticeMenor(triangulo.getVertice1());
                triangulo.setVerticeMeio(triangulo.getVertice2());
                triangulo.setVerticeMaior(triangulo.getVertice3());
            } else {
                if (tri1 < tri2 && tri2 <= tri3) {
                    triangulo.setVerticeMenor(triangulo.getVertice1());
                    triangulo.setVerticeMeio(triangulo.getVertice2());
                    triangulo.setVerticeMaior(triangulo.getVertice3());
                } else {
                    if (tri1 < tri3 && tri3 <= tri2) {
                        triangulo.setVerticeMenor(triangulo.getVertice1());
                        triangulo.setVerticeMeio(triangulo.getVertice3());
                        triangulo.setVerticeMaior(triangulo.getVertice2());
                    } else {
                        if (tri2 < tri1 && tri1 <= tri3) {
                            triangulo.setVerticeMenor(triangulo.getVertice2());
                            triangulo.setVerticeMeio(triangulo.getVertice1());
                            triangulo.setVerticeMaior(triangulo.getVertice3());
                        } else {
                            if (tri2 < tri3 && tri3 <= tri1) {
                                triangulo.setVerticeMenor(triangulo.getVertice2());
                                triangulo.setVerticeMeio(triangulo.getVertice3());
                                triangulo.setVerticeMaior(triangulo.getVertice1());
                            } else {
                                if (tri3 < tri1 && tri1 <= tri2) {
                                    triangulo.setVerticeMenor(triangulo.getVertice3());
                                    triangulo.setVerticeMeio(triangulo.getVertice1());
                                    triangulo.setVerticeMaior(triangulo.getVertice2());
                                } else {
                                    triangulo.setVerticeMenor(triangulo.getVertice3());
                                    triangulo.setVerticeMeio(triangulo.getVertice2());
                                    triangulo.setVerticeMaior(triangulo.getVertice1());
                                }
                            }
                        }
                    }
                }
            }
            DAOTrianguloSingleton.getINSTANCE().addTriangulo(triangulo);
            contorno();
        }else{
            View view2 = null;
            ShowInsertVertices(view2);
        }

    }

    // Fim da inserção e remoção

    // Validadores

    public boolean validaAreaDelete(int intremove){
        System.out.println("Passou neste validador");
        boolean intremoveV=false;
        for (int i=0;i<DAOTrianguloSingleton.getINSTANCE().getTriangulo().size();i++) {
            if(i==intremove-1){
                intremoveV=true;
                System.out.println("Passou neste validador 2");
            }
        }
        if(intremoveV){
            return true;
        }else{
            return false;
        }

    }

    public boolean validaVertice(int vertice1,int vertice2,int vertice3){
        boolean vertice1V=false;
        boolean vertice2V=false;
        boolean vertice3V=false;
        for (Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()) {
            if(topo.getPonto()==vertice1){
                vertice1V=true;
            }
            if(topo.getPonto()==vertice2){
                vertice2V=true;
            }
            if(topo.getPonto()==vertice3){
                vertice3V=true;
            }
        }
        if(vertice1V && vertice2V && vertice3V){
            return true;
        }else{
            if(vertice1V==false && vertice2V==true && vertice3V==true){

            }
            if(vertice2V==false && vertice1V==true && vertice3V==true){

            }
            if(vertice3V==false && vertice2V==true && vertice3V==true){

            }
            if(vertice1V==false && vertice2V==false && vertice3V==true){

            }
            if(vertice1V==false && vertice3V==false && vertice2V==true){

            }
            if(vertice2V==false && vertice3V==false && vertice1V==true){

            }
            if(vertice1V==false && vertice2V==false && vertice3V==false){
                String title="Problema na entrada dos tres vértices";
                showWarningVertices(title);
            }
            return false;
        }
    }

    // Fim dos validadores

    // Avisos

    public void showWarningVertices(String title) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(title);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogBuilder.create().show();
    }

    public void exporArea(View view2) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Deseja exportar o desenho da area para o Autocad?");

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                System.out.println("passou ja aqui");
                try {
                    gravarArquivoAutocad();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogBuilder.create().show();
    }

    public void ShowDeleteVertices(View view2) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Ecolha uma area para deletar");
        View view = getLayoutInflater().inflate(R.layout.survey_area_delete,null);
        dialogBuilder.setView(view);
        ImageView imvDeleteArea=view.findViewById(R.id.imvDeleteArea);
        TextView edtDeleteArea=view.findViewById(R.id.edtDeleteArea);
        contorno2(imvDeleteArea);

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int intremove;
                if(edtDeleteArea.getText().toString().equals("")) {
                    intremove = 1000000;
                }else{
                    intremove=Integer.parseInt(edtDeleteArea.getText().toString());
                }
                remocaoArea(intremove);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogBuilder.create().show();
    }

    public void ShowInsertVertices(View view2) {
        System.out.println("passou por esta view");
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("");
        View view = getLayoutInflater().inflate(R.layout.build_triangle_areas,null);
        dialogBuilder.setView(view);
        TextView edtVertice1=view.findViewById(R.id.edtVertice1);
        TextView edtVertice2=view.findViewById(R.id.edtVertice2);
        TextView edtVertice3=view.findViewById(R.id.edtVertice3);

        System.out.println("passou por esta view2");
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int vertice1=1000000;
                int vertice2=1000000;
                int vertice3=1000000;
                if(edtVertice1.getText().toString().equals("")){
                    vertice1=1000000;
                }else{
                    vertice1=Integer.parseInt(edtVertice1.getText().toString());
                }
                if(edtVertice2.getText().toString().equals("")){
                    vertice2=1000000;
                }else{
                    vertice2=Integer.parseInt(edtVertice2.getText().toString());
                }
                if(edtVertice3.getText().toString().equals("")){
                    vertice3=1000000;
                }else{
                    vertice3=Integer.parseInt(edtVertice3.getText().toString());
                }
                System.out.println(vertice1+"  "+vertice2+"   "+vertice3);
                instanciaTriangulo(vertice1,vertice2,vertice3);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        dialogBuilder.create().show();
    }
    // Fim dos avisos

    // Exportar para arquivo

    //Gravar Gravar desenho


    public void gravarArquivoAutocad() throws IOException {

        builder.append("(defun c:AREATOPO( )\n");
        for(int i=1;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                double pt1X=DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaX();
                double pt1Y=DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaY();
                double pt2X=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX();
                double pt2Y=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY();
                builder.append("(setq pt1 '("+pt1X+" " +pt1Y+") pt2 '("+pt2X+" "+pt2Y+"))\n");
                builder.append("(command \"._line\" pt1 pt2 \"\")");
        }
        String testeCurvas=builder2.toString();
        if(!testeCurvas.equals("")){
            builder.append(testeCurvas);
        }
        builder.append(")");
        try {
            deleteFile(ARQUIVO);
            FileOutputStream out = openFileOutput(ARQUIVO,MODE_APPEND);
            out.write(builder.toString().getBytes());
            out.close();
        } catch (Exception e) {
            Log.e("ERRO", e.getMessage());
        }
    }
    //Fim Gravar desenho

    // Fim do exportar para arquivo

    public void onExit(View view){
        finish();
    }
}