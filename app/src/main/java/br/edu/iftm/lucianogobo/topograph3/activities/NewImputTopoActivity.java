package br.edu.iftm.lucianogobo.topograph3.activities;

import static android.app.PendingIntent.getActivity;
import static br.edu.iftm.lucianogobo.topograph3.activities.InputInitialActivities.numberOfVertices;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.edu.iftm.lucianogobo.topograph3.R;
import br.edu.iftm.lucianogobo.topograph3.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph3.model.Topo;
import br.edu.iftm.lucianogobo.topograph3.service.Service;

public class NewImputTopoActivity extends AppCompatActivity {

    public static final String RESULT_KEY="NewImputTopoActivity.RESULT_KEY";
    private TextView editPoint;
    private TextView editDirection;
    private TextView editAngularRe;
    private TextView editAngularVante;
    private TextView editZenit;
    private TextView editLineMinus;
    private TextView editLineMedium;
    private TextView editLineHight;
    private TextView editAltTeodolite;
    static int contador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_imput_topo);
        this.editPoint = findViewById(R.id.editTextNumber2);
        this.editDirection = findViewById(R.id.editTextTextPersonName7);
        this.editAngularRe = findViewById(R.id.editTextTextPersonName2);
        this.editAngularVante = findViewById(R.id.editTextTextPersonName3);
        this.editZenit = findViewById(R.id.editTextTextPersonName4);
        this.editLineMinus = findViewById(R.id.editTextTextPersonName5);
        this.editLineMedium = findViewById(R.id.editTextTextPersonName8);
        this.editLineHight = findViewById(R.id.editTextTextPersonName6);
        this.editAltTeodolite = findViewById(R.id.editTextNumberDecimal);
 //       contador=0;
        contador=DAOTopoSingleton.getINSTANCE().getTopos().size();
        System.out.println("O valor do contador e "+contador);
    }

    public void onSave(View view) {
        int NumeroVertices = numberOfVertices;
        if(contador==NumeroVertices){
            Intent newTableData = new Intent(this,TopoTableOne.class);
            startActivity(newTableData);
            finish();
        }
        contador = contador + 1;
        String point = this.editPoint.getText().toString();
        int ponto = Integer.parseInt(point);
        String direction = this.editDirection.getText().toString();
        String angularRe;
        String angularVante;
        String zenit;
        String lineMinus;
        String lineMedium;
        String lineHight;
        String altTeodolite;
        if(formatoAngulo(this.editAngularRe.getText().toString())){
            angularRe = this.editAngularRe.getText().toString();
        }else{
            contador = contador - 1;
            editAngularRe.setText("");
            editAngularRe.requestFocus();
            return;
        }
        if(formatoAngulo(this.editAngularVante.getText().toString())){
            angularVante = this.editAngularVante.getText().toString();
        }else{
            contador = contador - 1;
            editAngularVante.setText("");
            editAngularVante.requestFocus();
            return;
        }
        if(formatoAngulo(this.editZenit.getText().toString())){
            zenit = this.editZenit.getText().toString();
        }else{
            contador = contador - 1;
            editZenit.setText("");
            editZenit.requestFocus();
            return;
        }
        if(Double.parseDouble(this.editLineMinus.getText().toString())>=0.0){
            lineMinus = this.editLineMinus.getText().toString();
        }else{
            contador = contador - 1;
            showAnguloInfo("A medida do fio inferior nao pode ser negativa");
            editLineMinus.setText("");
            editLineMinus.requestFocus();
            return;
        }
        if(Double.parseDouble(this.editLineMedium.getText().toString())>=0.0){
            lineMedium = this.editLineMedium.getText().toString();
        }else{
            contador = contador - 1;
            showAnguloInfo("A medida do fio medio nao pode ser negativa");
            editLineMedium.setText("");
            editLineMedium.requestFocus();
            return;
        }
        if(Double.parseDouble(this.editLineHight.getText().toString())>=0.0){
            lineHight = this.editLineHight.getText().toString();
        }else{
            contador = contador - 1;
            showAnguloInfo("A medida do fio superior nao pode ser negativa");
            editLineHight.setText("");
            editLineHight.requestFocus();
            return;
        }
        if(Double.parseDouble(this.editAltTeodolite.getText().toString())>=0.0){
            altTeodolite = this.editAltTeodolite.getText().toString();
        }else{
            contador = contador - 1;
            showAnguloInfo("A altura do teodolito nao pode ser negativa");
            editAltTeodolite.setText("");
            editAltTeodolite.requestFocus();
            return;
        }
        double alturaTeodolito = Double.parseDouble(altTeodolite);
        String anguloHorizontal="0.00'00\"";
        double distancia = 0.00;
        String anguloCorrigido = "nao_disponivel";
        String azimute = "nao_disponivel";
        double distanciaX = 0.00;
        double distanciaY = 0.00;
        double distanciaZ = 0.00;
        double cota = 0.00;
        double deltaH = 0.00;
        if(contador!=1){
            AreaActivity areaActivity=new AreaActivity();
            double angularReD=areaActivity.anguloFormatoDecimal(angularRe);
            double angularVanteD=areaActivity.anguloFormatoDecimal(angularVante);
            double angulohorizontalD=angularVanteD-angularReD;
            anguloHorizontal=areaActivity.anguloFormatoSexadecimal(angulohorizontalD);
     //       showAnguloInfo(anguloHorizontal);
        }

        if (point.isEmpty() || direction.isEmpty() || angularRe.isEmpty() ||
                angularVante.isEmpty() || zenit.isEmpty() || lineMinus.isEmpty() ||
                lineMedium.isEmpty() || lineHight.isEmpty() || altTeodolite.isEmpty()) {
            contador = contador - 1;
            return;
        }

        Topo topo = new Topo(ponto, direction, angularRe, angularVante, zenit, lineMinus, lineMedium,
                lineHight, alturaTeodolito, anguloHorizontal, distancia, anguloCorrigido,
                azimute, distanciaX, distanciaY, distanciaZ, cota, deltaH);

        DAOTopoSingleton.getINSTANCE().addStore(topo);
        editPoint.setText("");
        editDirection.setText("");
        editAngularRe.setText("");
        editAngularVante.setText("");
        editZenit.setText("");
        editLineMinus.setText("");
        editLineMedium.setText("");
        editLineHight.setText("");
        editAltTeodolite.setText("");
        if(contador == NumeroVertices){
            Intent newTableData = new Intent(this,TopoTableOne.class);
            startActivity(newTableData);
            finish();
        }
        editPoint.requestFocus();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    public void showAnguloInfo(String validador) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Erro na entrada de dados");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setMessage(validador)
                .setPositiveButton(R.string.btnOK, (dialog, id) -> {
                    // FIRE ZE MISSILES!
                      dialog.cancel();
            //        Toast.makeText(NewImputTopoActivity.this, R.string.btnOK + id, Toast.LENGTH_SHORT).show();
                });
        /*        .setNegativeButton(R.string.btnCancel, (dialog, id) -> {
                      dialog.cancel();
          //          Toast.makeText(NewImputTopoActivity.this, R.string.btnCancel + id, Toast.LENGTH_SHORT).show();
                });

  /*      dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        }); */
        dialogBuilder.create();
        dialogBuilder.show();
    }


    public void correction(View view){
        Intent newTableData = new Intent(this,TopoTableOne.class);
        startActivity(newTableData);
    }

    public void showDesignSurvey(View view) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Image of the survey");
        View view2 = getLayoutInflater().inflate(R.layout.design_survey,null);
        dialogBuilder.setView(view2);
        ImageView imageView =view2.findViewById(R.id.imageView3);
        Service service=new Service();
        service.coordenadasP();
        imageView.setImageBitmap(service.poligonal());
        dialogBuilder.setCancelable(false);
        dialogBuilder.setMessage("")
                .setPositiveButton(R.string.btnOK, (dialog, id) -> {
                    // FIRE ZE MISSILES!
                    dialog.cancel();
                    //        Toast.makeText(NewImputTopoActivity.this, R.string.btnOK + id, Toast.LENGTH_SHORT).show();
                });
        /*        .setNegativeButton(R.string.btnCancel, (dialog, id) -> {
                      dialog.cancel();
          //          Toast.makeText(NewImputTopoActivity.this, R.string.btnCancel + id, Toast.LENGTH_SHORT).show();
                });

  /*      dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        }); */
        dialogBuilder.create();
        dialogBuilder.show();
    }

    public boolean formatoAngulo(String angulo){
        if(!angulo.substring(0,4).matches("[0-9]*")){
            if(angulo.substring(0,3).matches("[0-9]*") ){
                int angulonumero=Integer.parseInt(angulo.substring(0,3));
                if(angulonumero<=360 && angulonumero>=0){
                    if(angulo.substring(3,4).matches("[.]")){
                        if(angulo.substring(4,6).matches("[0-9]*")){
                            if(angulo.substring(4,5).matches("[0-5]*")){
                                if(angulo.substring(6,7).matches("[']")){
                                    if(angulo.substring(7,9).matches("[0-9]*")){
                                        if(angulo.substring(7,8).matches("[0-5]*")){
                                            if(angulo.substring(9).matches("[\"]")){
                                                return true;
                                            }else{
                                                showAnguloInfo("Atrás dos segundos deve ter o simbolo  \" ");
                                                return false;
                                            }
                                        }else{
                                            showAnguloInfo("A dezena dos segundos deve variar entre zero e cinco");
                                            return false;
                                        }
                                    }else{
                                        showAnguloInfo("Os segundos devem ser numeros naturais");
                                        return false;
                                    }
                                }else{
                                    showAnguloInfo("Depois dos minutos, deve vir o simbolo ' ");
                                    return false;
                                }
                            }else{
                                showAnguloInfo("A dezena dos minutos só pode variar entre 0 e 5");
                                return false;
                            }
                        }else{
                            showAnguloInfo("Não pode haver letras nos minutos");
                            return false;
                        }

                    }else{
                        showAnguloInfo("Depois do angulo, deve vir o símbolo °");
                        return false;
                    }
                }else{
                    showAnguloInfo("O angulo não pode ser maior que 360 nem menor que zero");
                    return false;
                }
            }else{
                if(angulo.substring(0,2).matches("[0-9]*")){
              //      System.out.println(angulo.substring(2,3));
                    if(angulo.substring(2,3).matches("[.]")){
                        if(angulo.substring(3,5).matches("[0-9]*")){
                            if(angulo.substring(3,4).matches("[0-5]*")){
                                if(angulo.substring(5,6).matches("[']")){
                                    if(angulo.substring(6,8).matches("[0-9]*")){
                                        if(angulo.substring(6,7).matches("[0-5]*")){
                                            if(angulo.substring(8).matches("[\"]")){
                                                System.out.println("O angulo é valido!");
                                                return true;
                                            }else{
                                                showAnguloInfo("Atrás dos segundos deve ter o simbolo \" ");
                                                return false;
                                            }
                                        }else{
                                            showAnguloInfo("A dezena dos segundos deve variar entre 0 e 5");
                                            return false;
                                        }
                                    }else{
                                        showAnguloInfo("Os segundos devem ser numeros naturais");
                                        return false;
                                    }
                                }else{
                                    showAnguloInfo("Depois do minuto, deve haver o símbolo '");
                                    return false;
                                }
                            }else{
                                showAnguloInfo("A dezena dos minutos só pode variar entre 0 e 5");
                                return false;
                            }
                        }else{
                            showAnguloInfo("Os minutos devem ser numeros naturais");
                            return false;
                        }
                    }else{
                        showAnguloInfo("Depois do angulo, deve vir o símbolo °");
                        return false;
                    }
                }else{
                    if(angulo.substring(0,1).matches("[0-9]*")){
                        System.out.println(angulo.substring(1,2));
                        if(angulo.substring(1,2).matches("[.]")){
                            if(angulo.substring(2,4).matches("[0-9]*")){
                                if(angulo.substring(2,3).matches("[0-5]*")){
                                    if(angulo.substring(4,5).matches("[']")){
                                        if(angulo.substring(5,7).matches("[0-9]*")){
                                            if(angulo.substring(5,6).matches("[0-5]*")){
                                                if(angulo.substring(7).matches("[\"]")){
                                                    System.out.println("O angulo e valido!");
                                                    return true;
                                                }else{
                                                    showAnguloInfo("Atrás dos segundos deve ter o simbolo \" ");
                                                    return false;
                                                }
                                            }else{
                                                showAnguloInfo("A dezena dos segundos deve variar entre 0 e 5");
                                                return false;
                                            }
                                        }else{
                                            showAnguloInfo("Os segundos devem ser numeros naturais");
                                            return false;
                                        }
                                    }else{
                                        showAnguloInfo("Depois do minuto, deve haver o símbolo '");
                                        return false;
                                    }
                                }else{
                                    showAnguloInfo("A dezena dos minutos só pode variar entre 0 e 5");
                                    return false;
                                }
                            }else{
                                showAnguloInfo("Os minutos devem ser numeros naturais");
                                return false;
                            }
                        }else{
                            showAnguloInfo("Depois do angulo, deve vir o símbolo °");
                            return false;
                        }
                    }else{
                        showAnguloInfo("O angulo deve ser um numero natural");
                        return false;
                    }
                }
            }
        }else{
            showAnguloInfo("O angulo não pode ser maior que tres digitos");
            return false;
        }
    }

}