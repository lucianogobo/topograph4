package br.edu.iftm.lucianogobo.topograph3.activities;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

import br.edu.iftm.lucianogobo.topograph3.model.Coordenadas;

public class Area2 extends View {

    Paint paint = new Paint();

    private void init(){

        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(8f);
        paint.setTextSize(48f);
    }

    public Area2 (Context context){
        super(context);
        init();
    }

    public Area2 (Context context, AttributeSet attrs, int defStyle){
        super(context,attrs,defStyle);
        init();
    }

    @Override
    public void onDraw (Canvas canvas){
     //   canvas.drawLine(0,0,200,200,paint);
     //   canvas.drawLine(200,0,0,200,paint);

        ArrayList<Coordenadas> coord = new ArrayList<Coordenadas>();
        Coordenadas coordenadas = new Coordenadas();
        coordenadas.setX(224.19);
        coordenadas.setY(589.25);
        coord.add(coordenadas);

        Coordenadas coordenadas1 = new Coordenadas();
        coordenadas1.setX(320.05);
        coordenadas1.setY(560.22);
        coord.add(coordenadas1);

        Coordenadas coordenadas2 = new Coordenadas();
        coordenadas2.setX(332.82);
        coordenadas2.setY(445.17);
        coord.add(coordenadas2);

        Coordenadas coordenadas3 = new Coordenadas();
        coordenadas3.setX(220.03);
        coordenadas3.setY(415.32);
        coord.add(coordenadas3);

        Coordenadas coordenadas4 = new Coordenadas();
        coordenadas4.setX(246.67);
        coordenadas4.setY(503.04);
        coord.add(coordenadas4);

        Coordenadas coordenadas5 = new Coordenadas();
        coordenadas5.setX(224.19);
        coordenadas5.setY(589.25);
        coord.add(coordenadas5);

        double fatorescala;
        double comprimentoPadrao;
        double xmax;
        double ymax;
        double xmin;
        double ymin;
        double xMedio;
        double yMedio;
        double xMedia;
        double yMedia;
        double comprimentoX;
        double comprimentoY;
        comprimentoX=canvas.getWidth();
        comprimentoY=canvas.getHeight();
        if(comprimentoX>=comprimentoY){
            comprimentoPadrao=comprimentoY;
        }else{
            comprimentoPadrao=comprimentoX;
        }
        xmax=0.0;
        ymax=0.0;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()>xmax){
                xmax=coord.get(i).getX();
            }
            if(coord.get(i).getY()>ymax){
                ymax=coord.get(i).getY();
            }
        }
        xmin=xmax;
        ymin=ymax;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()<xmin){
                xmin=coord.get(i).getX();
            }
            if(coord.get(i).getY()<ymin){
                ymin=coord.get(i).getY();
            }
        }
        xMedio=(xmax-xmin)/2.0;
        yMedio=(ymax-ymin)/2.0;
        xMedia=(xmax+xmin)/2.0;
        yMedia=(ymax+ymin)/2.0;
        if(xMedio>=yMedio){
            fatorescala=(comprimentoPadrao/2.0)/xMedio;
        }else{
            fatorescala=(comprimentoPadrao/2.0)/yMedio;
        }
        int xPartida=(int)comprimentoX/2;
        int yPartida=(int)comprimentoY/2;
        for(int i=1;i<=coord.size();i++){
            float xInicial=xPartida+((float)coord.get(i-1).getX()-(float)xMedia)*(float)fatorescala;
            float yInicial=yPartida-((int)coord.get(i-1).getY()-(float)yMedia)*(float)fatorescala;
            float xFinal;
            float yFinal;
            if(i==coord.size()){
                xFinal=xPartida+((float)coord.get(0).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=yPartida-((float)coord.get(0).getY()-(float)yMedia)*(float)fatorescala;
            }else{
                xFinal=(int)xPartida+((float)coord.get(i).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=(int)yPartida-((float)coord.get(i).getY()-(float)yMedia)*(float)fatorescala;
                canvas.drawLine(xInicial,yInicial,xFinal,yFinal,paint);
                if(i<coord.size()){
                    String ponto=Integer.toString(i);
         //           canvas.drawText(ponto, xInicial+5,yInicial-20,paint);
                    ponto ="("+Double.toString(coord.get(i-1).getX())+","
                            +Double.toString(coord.get(i-1).getY())+")";
                    canvas.drawText(ponto, xInicial+5,yInicial-5,paint);
                }
            }
        }
    }

}
