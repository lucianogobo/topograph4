package br.edu.iftm.lucianogobo.topograph3.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Topo implements Parcelable {
    private int ponto;
    private String direcao;
    private String anguloRE;
    private String anguloVANTE;
    private String zenite;
    private String fioInferior;
    private String fioMedio;
    private String fioSuperior;
    private double alturaAparelho;
    private String anguloHorizontal;
    private double distancia;
    private String anguloCorrigido;
    private String azimute;
    private double distanciaX;
    private double distanciaY;
    private double distanciaZ;
    private double cota;
    private double deltaH;

    public Topo (int ponto,String direcao,String anguloRE,String anguloVANTE,
                 String zenite,String fioInferior,String fioMedio,String fioSuperior,
                 double alturaAparelho,String anguloHorizontal,double distancia,
                 String anguloCorrigido,String azimute,double distanciaX,
                 double distanciaY,double distanciaZ,double cota,double deltaH){

        this.ponto=ponto;
        this.direcao=direcao;
        this.anguloRE=anguloRE;
        this.anguloVANTE=anguloVANTE;
        this.zenite=zenite;
        this.fioInferior=fioInferior;
        this.fioMedio=fioMedio;
        this.fioSuperior=fioSuperior;
        this.alturaAparelho=alturaAparelho;
        this.anguloHorizontal=anguloHorizontal;
        this.distancia=distancia;
        this.anguloCorrigido=anguloCorrigido;
        this.azimute=azimute;
        this.distanciaX=distanciaX;
        this.distanciaY=distanciaY;
        this.distanciaZ=distanciaZ;
        this.cota=cota;
        this.deltaH=deltaH;

    }

    public Topo(){

    }


    public int getPonto() {
        return ponto;
    }

    public String getDirecao() {
        return direcao;
    }

    public String getAnguloRE() {
        return anguloRE;
    }

    public String getAnguloVANTE() {
        return anguloVANTE;
    }

    public String getZenite() {
        return zenite;
    }

    public String getFioInferior() {
        return fioInferior;
    }

    public String getFioMedio() {
        return fioMedio;
    }

    public String getFioSuperior() {
        return fioSuperior;
    }

    public double getAlturaAparelho() {
        return alturaAparelho;
    }

    public String getAnguloHorizontal() {
        return anguloHorizontal;
    }

    public double getDistancia() {
        return distancia;
    }

    public String getAnguloCorrigido() {
        return anguloCorrigido;
    }

    public String getAzimute() {
        return azimute;
    }

    public double getDistanciaX() {
        return distanciaX;
    }

    public double getDistanciaY() {
        return distanciaY;
    }

    public double getDistanciaZ() {
        return distanciaZ;
    }

    public double getCota() {
        return cota;
    }

    public double getDeltaH() {
        return deltaH;
    }


    public int setPonto(int ponto) {
        return this.ponto=ponto;
    }

    public String setDirecao(String direcao) {
        return this.direcao=direcao;
    }

    public String setAnguloRE(String anguloRE) {
        return this.anguloRE=anguloRE;
    }

    public String setAnguloVANTE(String anguloVANTE) {
        return this.anguloVANTE= anguloVANTE;
    }

    public String setZenite(String zenite) {
        return this.zenite= zenite;
    }

    public String setFioInferior(String fioInferior) {
        return this.fioInferior=fioInferior;
    }

    public String setFioMedio(String fioMedio) {
        return this.fioMedio=fioMedio;
    }

    public String setFioSuperior(String fioSuperior) {
        return this.fioSuperior=fioSuperior;
    }

    public double setAlturaAparelho(Double alturaAparelho) {
        return this.alturaAparelho=alturaAparelho;
    }

    public String setAnguloHorizontal(String anguloHorizontal) {
        return this.anguloHorizontal=anguloHorizontal;
    }

    public double setDistancia(Double distancia) {
        return this.distancia=distancia;
    }

    public String setAnguloCorrigido(String anguloCorrigido) {
        return this.anguloCorrigido=anguloCorrigido;
    }

    public String setAzimute(String azimute) {
        return this.azimute=azimute;
    }

    public double setDistanciaX(Double distanciaX) {
        return this.distanciaX=distanciaX;
    }

    public double setDistanciaY(Double distanciaY) {
        return this.distanciaY=distanciaY;
    }

    public double setDistanciaZ(Double distanciaZ) {
        return this.distanciaZ=distanciaZ;
    }

    public double setCota(Double cota) {
        return this.cota=cota;
    }

    public double setDeltaH(Double deltaH) {
        return this.deltaH=deltaH;
    }

//-----------Parcelable-----------------------------------------------------

    protected Topo(Parcel in) {
        ponto = in.readInt();
        direcao = in.readString();
        anguloRE = in.readString();
        anguloVANTE = in.readString();
        zenite = in.readString();
        fioInferior = in.readString();
        fioMedio = in.readString();
        fioSuperior = in.readString();
        alturaAparelho = in.readDouble();
        anguloHorizontal = in.readString();
        distancia = in.readDouble();
        anguloCorrigido = in.readString();
        azimute = in.readString();
        distanciaX = in.readDouble();
        distanciaY = in.readDouble();
        distanciaZ = in.readDouble();
        cota = in.readDouble();
        deltaH = in.readDouble();
    }

    public static final Creator<Topo> CREATOR = new Creator<Topo>() {
        @Override
        public Topo createFromParcel(Parcel in) {
            return new Topo(in);
        }

        @Override
        public Topo[] newArray(int size) {
            return new Topo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(ponto);
        dest.writeString(direcao);
        dest.writeString(anguloRE);
        dest.writeString(anguloVANTE);
        dest.writeString(zenite);
        dest.writeString(fioInferior);
        dest.writeString(fioMedio);
        dest.writeString(fioSuperior);
        dest.writeDouble(alturaAparelho);
        dest.writeString(anguloHorizontal);
        dest.writeDouble(distancia);
        dest.writeString(anguloCorrigido);
        dest.writeString(azimute);
        dest.writeDouble(distanciaX);
        dest.writeDouble(distanciaY);
        dest.writeDouble(distanciaZ);
        dest.writeDouble(cota);
        dest.writeDouble(deltaH);
    }
}
