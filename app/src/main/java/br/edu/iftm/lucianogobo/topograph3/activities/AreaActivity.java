package br.edu.iftm.lucianogobo.topograph3.activities;

import static br.edu.iftm.lucianogobo.topograph3.activities.InputInitialActivities.azimuthFirst;
import static br.edu.iftm.lucianogobo.topograph3.activities.InputInitialActivities.numberOfVertices;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import br.edu.iftm.lucianogobo.topograph3.R;
import br.edu.iftm.lucianogobo.topograph3.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph3.model.Coordenadas;
import br.edu.iftm.lucianogobo.topograph3.model.Topo;
import br.edu.iftm.lucianogobo.topograph3.service.Service;

public class AreaActivity extends AppCompatActivity {

    private double areaPoligonal;
    private TextView editArea;
    private ImageView imageView;
    private Button button;
    final String ARQUIVO = "relatorio.txt";
    private StringBuilder builder = new StringBuilder("");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area);
        this.editArea=findViewById(R.id.editTextTextPersonName17);
        this.imageView=(ImageView) findViewById(R.id.imageView);
        this.button=findViewById(R.id.button13);
        int tamanho=DAOTopoSingleton.getINSTANCE().sendSize();
        if(tamanho==numberOfVertices) {
            impressao(1,0.0,0.0,0.0,0.0,
                    0.0,0.0,0.0,0.0,0.0,0.0);
            estadiometria();
            correcao();
            azimute();
            impressao(4,0.0,0.0,0.0,0.0,
                    0.0,0.0,0.0,0.0,0.0,0.0);
            coordenadas();
            area();
            poligonal();
        }else{
            showAreaInfo("You have to complete the the topographic survey");
        }

    }

    public void correcao() {
        double soma;
        double difangulos;
        double difparcial;
        double somaangulos;
        soma = 0.0;
        double anguloDV[] = new double[DAOTopoSingleton.getINSTANCE().getTopos().size()];
        int i = 0;
        somaangulos = (numberOfVertices + 1) * 180;
        for (Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()) {
            if (i != 0) {
                double anguloD = anguloFormatoDecimal(topo.getAnguloHorizontal());
                System.out.println("angulo decimal= "+anguloD);
                anguloDV[i] = anguloD;
                soma = soma + anguloD;
            }
            i++;
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println("angulo sexadecimal= "+topo.getAnguloHorizontal());
        }

        if (soma != somaangulos) {
            double teleranceAngle=(0.036*Math.sqrt(numberOfVertices-1));
            difangulos = soma - somaangulos;
            impressao(2,somaangulos,soma,teleranceAngle,difangulos,0.0,
                    0.0,0.0,0.0,0.0,0.0);
            if(Math.abs(difangulos)<teleranceAngle) {
                difparcial = difangulos / (numberOfVertices - 1);
                for (int j = 1; j < DAOTopoSingleton.getINSTANCE().getTopos().size(); j++) {
                    anguloDV[j] = anguloDV[j] - difparcial;
                }
                soma = 0.0;
                for (int j = 0; j < DAOTopoSingleton.getINSTANCE().getTopos().size(); j++) {
                    soma = soma + anguloDV[j];
                }
            }else{
                showAreaInfo("The error in the sum of angles is major than the tolerance,topographic survay invalid");
            }
        }
        i = 0;
        for (Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()) {
            if (i != 0) {
                String anguloSD = anguloFormatoSexadecimal(anguloDV[i]);
                topo.setAnguloCorrigido(anguloSD);
            }else{
                topo.setAnguloCorrigido("0.00'00\"");
            }
            i++;
        }
        impressao(3,0.0,0.0,0.0,0.0,
                0.0,0.0,0.0,0.0,0.0,0.0);
    }

    public void estadiometria(){
        for(Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()){
            double zeniteD=anguloFormatoDecimal(topo.getZenite());
            double alpha=(90.0-zeniteD)*Math.PI/180;
            double fioInferiorD=Double.parseDouble(topo.getFioInferior());
            double fioMedioD=Double.parseDouble(topo.getFioMedio());
            double fioSuperiorD=Double.parseDouble(topo.getFioSuperior());
            double H =fioSuperiorD-fioInferiorD;
            double dist =100.0*H*Math.pow(Math.cos(alpha), 2);
            topo.setDistancia(dist);
            System.out.println("======================================================");
            System.out.println("fio inferior = "+fioInferiorD);
            System.out.println("fio superior = "+fioSuperiorD);
        }
    }

    public void azimute(){
        String azimuthini=azimuthFirst;
        for(int i = 0; i < DAOTopoSingleton.getINSTANCE().getTopos().size(); i++){
            if(i==0){
                DAOTopoSingleton.getINSTANCE().getTopos().get(i).setAzimute(azimuthini);
            }else{
                double azimuteAntD=anguloFormatoDecimal(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getAzimute());
                double anguloCorrigidoD=anguloFormatoDecimal(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloCorrigido());
                double azimuteD=(azimuteAntD+anguloCorrigidoD-180);
                //           String angulohorizontalD=Double.toString(azimuteAntD);
                //           showAnguloInfoA(angulohorizontalD,"azimute anterior");

                if(azimuteD>360){
                    azimuteD=azimuteD-360.0;
                }
                if(azimuteD<0.0){
                    azimuteD=azimuteD+360.0;
                }
                DAOTopoSingleton.getINSTANCE().getTopos().get(i).setAzimute(anguloFormatoSexadecimal(azimuteD));
            }
        }
    }

    public void coordenadas(){
        double X=0.0;
        double Y=0.0;
        double Z;
        double perimetro;
        double erroX;
        double erroY;
        double erro;
        double erroTolerancia;
        double tol;
        Service service=new Service();
        service.coordenadasP();
        for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
            if(i==0){
                X=224.19;
                Y=589.25;
            }else{
                double distanciaAnt=DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistancia();
                double azimuteAnt=anguloFormatoDecimal(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getAzimute());
                X=X+distanciaAnt*Math.sin(Math.toRadians(azimuteAnt));
                Y=Y+distanciaAnt*Math.cos(Math.toRadians(azimuteAnt));
            }
            DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDistanciaX(X);
            DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDistanciaY(Y);
        }
        System.out.println("***********************************");
        System.out.println(DAOTopoSingleton.getINSTANCE().getTopos().get(0).getDistanciaX());
        System.out.println("***********************************");
        double xInicial=DAOTopoSingleton.getINSTANCE().getTopos().get(0).getDistanciaX();
        double xFinal=DAOTopoSingleton.getINSTANCE().getTopos().
                get(DAOTopoSingleton.getINSTANCE().getTopos().size()-1).getDistanciaX();
        double yInicial=DAOTopoSingleton.getINSTANCE().getTopos().get(0).getDistanciaY();
        double yFinal=DAOTopoSingleton.getINSTANCE().getTopos().
                get(DAOTopoSingleton.getINSTANCE().getTopos().size()-1).getDistanciaY();
        erroX=xFinal-xInicial;
        erroY=yFinal-yInicial;
        erro=Math.sqrt(Math.pow(erroX, 2)+Math.pow(erroY, 2));
        perimetro=0.0;
        for(Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()){
            perimetro=perimetro+topo.getDistancia();
        }
        Z=perimetro/erro;
        erroTolerancia=1.0/Z;
        tol=0.0005;
        impressao(5,0.0,0.0,0.0,0.0,
                erroX,erroY,erro,perimetro,erroTolerancia,0.0);
        if(erroTolerancia<tol){
            for(int i=1;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                double xAnt=DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaX();
                double yAnt=DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaY();
                double distanciaAnt=DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistancia();
                double azimuteAnt=anguloFormatoDecimal(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getAzimute());
                X=xAnt+distanciaAnt*Math.sin(Math.toRadians(azimuteAnt))-erroX*distanciaAnt/perimetro;
                Y=yAnt+distanciaAnt*Math.cos(Math.toRadians(azimuteAnt))-erroY*distanciaAnt/perimetro;
                DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDistanciaX(X);
                DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDistanciaY(Y);
            }
            erroX=DAOTopoSingleton.getINSTANCE().getTopos().get(
                    DAOTopoSingleton.getINSTANCE().getTopos().size()-1).getDistanciaX()-
                    DAOTopoSingleton.getINSTANCE().getTopos().get(0).getDistanciaX();
            erroY=DAOTopoSingleton.getINSTANCE().getTopos().get(
                    DAOTopoSingleton.getINSTANCE().getTopos().size()-1).getDistanciaY()-
                    DAOTopoSingleton.getINSTANCE().getTopos().get(0).getDistanciaY();
            erro=Math.sqrt(Math.pow(erroX, 2)+Math.pow(erroY, 2));
            impressao(6,0.0,0.0,0.0,0.0,
                    erroX,erroY,erro,perimetro,erroTolerancia,0.0);
        }else{
            showAreaInfo("The error is major then the tolerance, invalid topographic survey");
            finish();
        }
    }

    public  void area(){
        double area1=0.0;
        for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size()-1;i++){
            double xAnt=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX();
            double xPos=DAOTopoSingleton.getINSTANCE().getTopos().get(i+1).getDistanciaX();
            double yAnt=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY();
            double yPos=DAOTopoSingleton.getINSTANCE().getTopos().get(i+1).getDistanciaY();
            area1=area1+0.5*(xAnt*yPos- xPos*yAnt);
        }
        area1=Math.sqrt(Math.pow(area1,2));
        String areaS=Double.toString(area1);
        //       String angulohorizontalD=Double.toString(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX());
        //       showAnguloInfoA(angulohorizontalD,"Coordenada X");
        this.editArea.setText(areaS);
        impressao(7,0.0,0.0,0.0,0.0,
                0.0,0.0,0.0,0.0,0.0,area1);

    }

    public void poligonal(){
        Bitmap bitmap = Bitmap.createBitmap(349,548,Bitmap.Config.ARGB_8888);
        bitmap=bitmap.copy(bitmap.getConfig(), true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4f);
        paint.setTextSize(14f);
        ArrayList<Coordenadas> coord = new ArrayList<Coordenadas>();
        for(Topo topo : DAOTopoSingleton.getINSTANCE().getTopos()){
            Coordenadas coordenadas = new Coordenadas();
            coordenadas.setX(topo.getDistanciaX());
            coordenadas.setY(topo.getDistanciaY());
            coordenadas.setZ(topo.getDistanciaZ());
            coord.add(coordenadas);
        }

        double fatorescala;
        double comprimentoPadrao;
        double xmax;
        double ymax;
        double xmin;
        double ymin;
        double xMedio;
        double yMedio;
        double xMedia;
        double yMedia;
        double comprimentoX;
        double comprimentoY;
        comprimentoX=canvas.getWidth();
        comprimentoY=canvas.getHeight();
        if(comprimentoX>=comprimentoY){
            comprimentoPadrao=comprimentoY;
        }else{
            comprimentoPadrao=comprimentoX;
        }
        xmax=0.0;
        ymax=0.0;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()>xmax){
                xmax=coord.get(i).getX();
            }
            if(coord.get(i).getY()>ymax){
                ymax=coord.get(i).getY();
            }
        }
        xmin=xmax;
        ymin=ymax;
        for(int i=0;i<coord.size();i++){
            if(coord.get(i).getX()<xmin){
                xmin=coord.get(i).getX();
            }
            if(coord.get(i).getY()<ymin){
                ymin=coord.get(i).getY();
            }
        }
        xMedio=(xmax-xmin)/2.0;
        yMedio=(ymax-ymin)/2.0;
        xMedia=(xmax+xmin)/2.0;
        yMedia=(ymax+ymin)/2.0;
        if(xMedio>=yMedio){
            fatorescala=(comprimentoPadrao/2.0)/xMedio;
        }else{
            fatorescala=(comprimentoPadrao/2.0)/yMedio;
        }
        int xPartida=(int)comprimentoX/2;
        int yPartida=(int)comprimentoY/2;
        for(int i=1;i<=coord.size();i++){
            float xInicial=xPartida+((float)coord.get(i-1).getX()-(float)xMedia)*(float)fatorescala;
            float yInicial=yPartida-((int)coord.get(i-1).getY()-(float)yMedia)*(float)fatorescala;
            float xFinal;
            float yFinal;
            if(i==coord.size()){
                xFinal=xPartida+((float)coord.get(0).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=yPartida-((float)coord.get(0).getY()-(float)yMedia)*(float)fatorescala;
            }else{
                xFinal=(int)xPartida+((float)coord.get(i).getX()-(float)xMedia)*(float)fatorescala;
                yFinal=(int)yPartida-((float)coord.get(i).getY()-(float)yMedia)*(float)fatorescala;
                canvas.drawLine(xInicial,yInicial,xFinal,yFinal,paint);
                if(i<coord.size()){
                    String ponto=Integer.toString(i);
                    //           canvas.drawText(ponto, xInicial+5,yInicial-20,paint);
                    String ponto1x = String.format("%.3f",coord.get(i-1).getX());
                    String ponto1y = String.format("%.3f",coord.get(i-1).getY());
                    ponto =ponto+ "("+ponto1x+"," +ponto1y+")";
                    canvas.drawText(ponto, xInicial+5,yInicial-5,paint);
                }
            }
        }
        imageView.setImageBitmap(bitmap);
    }

    // Impressão
    public void impressao(int ia,double somaangulos,double soma, double teleranceAngle,double difangulos,
                          double erroX,double erroY,double erro, double perimetro,double erroTolerancia,
                          double area){
        if(ia==1){
            builder.append("Tabela levantamento de campo: \n\n");
            builder.append("Ponto \t Ângulo Ré \t Ângulo Vante \t Zênite \t Fio inf. \t Fio Sup. \t Alt. Aparelho\n");
            builder.append("======================================================================================================\n");
            for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                int i1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto();
                String s1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloRE();
                String s2 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloVANTE();
                String s3 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getZenite();
                String s4 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getFioInferior();
                String s5 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getFioSuperior();
                String s6 = String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAlturaAparelho());
                builder.append(i1+"\t"+s1+" \t "+s2+"\t"+s3+"\t "+s4+"\t         "+s5+"    \t "+s6+" \n");
            }
            builder.append("\n\n");
        }
        if(ia==2){
            builder.append("Ângulo horizontal e distância entre os pontos: \n\n");
            builder.append("Ponto \t Direção \t Ângulo Horizontal \t Distância (m)\n");
            builder.append("==============================================================\n");
            for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                int i1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto();
                String s1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDirecao();
                String s2 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloHorizontal();
                String s3=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistancia());
                builder.append(i1+"\t "+s1+" \t         "+s2+"\t         "+s3+"\n");
            }
            builder.append("\n");
            builder.append("Somatório dos ângulos = 180°*(número de vertices +2) = "+somaangulos+"\n");
            builder.append("Somatório dos ângulos do levantamento = "+soma+"\n");
            builder.append("Tolerancia = 0,036*sqrt(número de vertices) = "+teleranceAngle+" \n");
            builder.append("Erro = "+difangulos+" \n");
            builder.append("Se Erro <= Tolerância, levantamento válido \n");
            builder.append("\n\n");
        }
        if(ia==3){
            builder.append("Ângulo horizontal corrigido e distância entre os pontos: \n\n");
            builder.append("Ponto \t Direção \t Ângulo Horizontal \t Distância (m)\n");
            builder.append("==============================================================\n");
            for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                int i1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto();
                String s1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDirecao();
                String s2 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloCorrigido();
                String s3=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistancia());
                builder.append(i1+"\t "+s1+" \t         "+s2+"\t         "+s3+"\n");
            }
            builder.append("\n\n");
        }
        if(ia==4){
            builder.append("Cálculo dos Azimutes: \n\n");
            builder.append("Ponto \t Direção \t Ângulo Hor. Corrigido \t Azimute\n");
            builder.append("==============================================================\n");
            for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                int i1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto();
                String s1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDirecao();
                String s2 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloCorrigido();
                String s3 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAzimute();
                builder.append(i1+"\t "+s1+" \t         "+s2+"\t         "+s3+"\n");
            }
            builder.append("\n\n");
        }
        if(ia==5){
            builder.append("Coordenadas Cartesianas dos pontos: \n\n");
            builder.append("Ponto \t    X(m) \t    Y(m) \t    Z(m)\n");
            builder.append("===================================================\n");
            for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                int i1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto();
                String s1=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX());
                String s2=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY());
                String s3=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaZ());
                builder.append(i1+"\t    "+s1+"\t    "+s2+"\t    "+s3+"\n");
            }
            builder.append("\n");
            builder.append("Erro linear : Diferença entre as coordenadas iniciais e finais \n");
            builder.append("Diferença coordenada X = "+erroX+"\n");
            builder.append("Diferença coordenada Y = "+erroY+" \n");
            builder.append("Erro = "+erro+" \n");
            builder.append("Perímetro (m) = "+perimetro+" \n");
            builder.append("Erro relativo = 1/Perímetro = "+erroTolerancia+"\n");
            builder.append("Erro relativo < Tolerância=0,0005 => Levantamento válido \n");
            builder.append("\n\n");
        }
        if(ia==6){
            builder.append("Coordenadas Cartesianas corrigidas dos pontos: \n\n");
            builder.append("Ponto \t    X(m) \t    Y(m)\n");
            builder.append("===================================================\n");
            for(int i=0;i<DAOTopoSingleton.getINSTANCE().getTopos().size();i++){
                int i1 = DAOTopoSingleton.getINSTANCE().getTopos().get(i).getPonto();
                String s1=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX());
                String s2=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY());
                String s3=String.format("%.3f",DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaZ());
                builder.append(i1+"\t    "+s1+"\t    "+s2+"\t    "+s3+"\n");
            }
            builder.append("\n");
            builder.append("Erro linear : Diferença entre as coordenadas iniciais e finais \n");
            builder.append("Diferença coordenada X = "+erroX+"\n");
            builder.append("Diferença coordenada Y = "+erroY+" \n");
            builder.append("Erro = "+erro+" \n");
            builder.append("Perímetro (m) = "+perimetro+" \n");
            builder.append("Erro relativo = 1/Perímetro = "+erroTolerancia+"\n");
            builder.append("Erro relativo < Tolerância=0,0005 => Levantamento válido \n");
            builder.append("\n\n");
        }
        if(ia==7){
            String s1 = String.format("%.3f",area);
            builder.append("Área (m2) ="+s1+" \n");
        }
    }

    // Fim da Impressão

    //Gravar Gravar desenho


    public void gravarArquivoTexto() throws IOException {

        try {
            deleteFile(ARQUIVO);
            FileOutputStream out = openFileOutput(ARQUIVO,MODE_APPEND);
            out.write(builder.toString().getBytes());
            out.close();
        } catch (Exception e) {
            Log.e("ERRO", e.getMessage());
        }
    }
    //Fim Gravar desenho

    // Transformarção de angulos

    public double anguloFormatoDecimal(String anguloS) {
        double angulo;
        double minuto;
        double segundo;
        if (anguloS.substring(0, 3).matches("[0-9]*")) {
            angulo = Double.parseDouble(anguloS.substring(0, 3));
            minuto = Double.parseDouble(anguloS.substring(4, 6));
            segundo = Double.parseDouble(anguloS.substring(7, 9));
        } else {
            if (anguloS.substring(0, 2).matches("[0-9]*")) {
                angulo = Double.parseDouble(anguloS.substring(0, 2));
                minuto = Double.parseDouble(anguloS.substring(3, 5));
                segundo = Double.parseDouble(anguloS.substring(6, 8));
            } else {
                angulo = Double.parseDouble(anguloS.substring(0, 1));
                minuto = Double.parseDouble(anguloS.substring(2, 4));
                segundo = Double.parseDouble(anguloS.substring(5, 7));
            }
        }
        double anguloreal = angulo + minuto / 60.0 + segundo / 3600.0;
        return anguloreal;
    }

    public String anguloFormatoSexadecimal(double anguloD) {
        double angulo;
        double decimal;
        double minuto;
        double segundo;
        int anguloN;
        int minutoN;
        int segundoN;
        String anguloR;
        String minutoR;
        String segundoR;
        angulo = Math.floor(anguloD);
        decimal = anguloD - Math.floor(anguloD);
        minuto = Math.floor(decimal * 60.0);
        decimal = (decimal * 60 - minuto);
        segundo = Math.floor(decimal * 60);
        anguloN = (int) angulo;
        minutoN = (int) minuto;
        segundoN = (int) segundo;
        anguloR = anguloN + "";
        if (minutoN >= 10) {
            minutoR = minutoN + "";
        } else {
            minutoR = "0" + minutoN + "";
        }
        if (segundoN >= 10) {
            segundoR = segundoN + "";
        } else {
            segundoR = "0" + segundoN + "";
        }
        anguloR = anguloR + "." + minutoR + "'" + segundoR + "\"";
        return anguloR;
    }

//  Fim da Transformarção de angulos




    public void deltaH(){
        for(int i=1;i<DAOTopoSingleton.getINSTANCE().getTopos().size()-1;i++){
            double cotgZ=   (Math.sin(Math.toRadians(Math.toRadians(
                             DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getAlturaAparelho())))/
                             Math.sin(Math.toRadians(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).
                                     getAlturaAparelho())));
            double deltaAga=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAlturaAparelho()-
                            DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getAlturaAparelho()+
                            DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistancia();
            DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDeltaH(deltaAga);
        }
    }

    // Avisos

    public void relatorioExpo(View view2) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Deseja exportar o relatório em um arquivo .txt?");

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    gravarArquivoTexto();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        dialogBuilder.create().show();
    }

    public void showAreaInfo(String dataInfo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Warning");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setMessage(dataInfo)
                .setPositiveButton(R.string.btnOK, (dialog, id) -> {
                    // FIRE ZE MISSILES!
                    dialog.cancel();
                    //        Toast.makeText(NewImputTopoActivity.this, R.string.btnOK + id, Toast.LENGTH_SHORT).show();
                });
        /*        .setNegativeButton(R.string.btnCancel, (dialog, id) -> {
                      dialog.cancel();
          //          Toast.makeText(NewImputTopoActivity.this, R.string.btnCancel + id, Toast.LENGTH_SHORT).show();
                });

  /*      dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        }); */
        dialogBuilder.create();
        dialogBuilder.show();
    }

    public void showAnguloInfoA(String validador,String titulo) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(titulo);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setMessage(validador)
                .setPositiveButton(R.string.btnOK, (dialog, id) -> {
                    // FIRE ZE MISSILES!
                    dialog.cancel();
                    //        Toast.makeText(NewImputTopoActivity.this, R.string.btnOK + id, Toast.LENGTH_SHORT).show();
                });
        /*        .setNegativeButton(R.string.btnCancel, (dialog, id) -> {
                      dialog.cancel();
          //          Toast.makeText(NewImputTopoActivity.this, R.string.btnCancel + id, Toast.LENGTH_SHORT).show();
                });

  /*      dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        }); */
        dialogBuilder.create();
        dialogBuilder.show();
    }

    // Fim dos Avisos

    public void onExit(View view){
        finish();
    }

}