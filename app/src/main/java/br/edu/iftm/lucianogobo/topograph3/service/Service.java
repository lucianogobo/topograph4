package br.edu.iftm.lucianogobo.topograph3.service;

import static br.edu.iftm.lucianogobo.topograph3.activities.InputInitialActivities.*;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.ArrayList;

import br.edu.iftm.lucianogobo.topograph3.activities.InputInitialActivities;
import br.edu.iftm.lucianogobo.topograph3.data.DAOTopoSingleton;
import br.edu.iftm.lucianogobo.topograph3.model.Coordenadas;
import br.edu.iftm.lucianogobo.topograph3.model.Topo;

public class Service {

    public double anguloFormatoDecimal(String anguloS) {
        double angulo;
        double minuto;
        double segundo;
        if (anguloS.substring(0, 3).matches("[0-9]*")) {
            angulo = Double.parseDouble(anguloS.substring(0, 3));
            minuto = Double.parseDouble(anguloS.substring(4, 6));
            segundo = Double.parseDouble(anguloS.substring(7, 9));
        } else {
            if (anguloS.substring(0, 2).matches("[0-9]*")) {
                angulo = Double.parseDouble(anguloS.substring(0, 2));
                minuto = Double.parseDouble(anguloS.substring(3, 5));
                segundo = Double.parseDouble(anguloS.substring(6, 8));
            } else {
                angulo = Double.parseDouble(anguloS.substring(0, 1));
                minuto = Double.parseDouble(anguloS.substring(2, 4));
                segundo = Double.parseDouble(anguloS.substring(5, 7));
            }
        }
        double anguloreal = angulo + minuto / 60.0 + segundo / 3600.0;
        return anguloreal;
    }


    public void coordenadasP(){


        int n =DAOTopoSingleton.getINSTANCE().getTopos().size();
        System.out.println(n);

        double X =0.0;
        double Y=0.0;
        double Z=0.0;
        double azimutes[] = new double[n];
        for(int i = 0; i< n; i++){
            if(i==0){
                X=224.19;
                Y=589.25;
                Z=500.00;
                azimutes[i]=anguloFormatoDecimal(azimuthSecond);
            }else{
                double zeniteD=anguloFormatoDecimal(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getZenite());
                System.out.println("Zenite"+zeniteD);
                double alpha=(90.0-zeniteD)*Math.PI/180;
                double fioInferiorD=Double.parseDouble(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getFioInferior());
                System.out.println("Fio inferior"+fioInferiorD);
                double fioMedioD=Double.parseDouble(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getFioMedio());
                double fioSuperiorD=Double.parseDouble(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getFioSuperior());
                double deltaAlturaAparelhos=DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getAlturaAparelho();
                System.out.println("Fio superior"+fioSuperiorD);
                double H =fioSuperiorD-fioInferiorD;
                double dist =100.0*H*Math.pow(Math.cos(alpha), 2);
                double distanciaAnt=dist;
                double azimuteAnt=azimutes[i-1];
                double cotangente;
                if(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getZenite().equals("90.00'00\"")){
                    cotangente=0.0;
                }else{
                    cotangente =1/Math.tan(Math.toRadians(zeniteD));
                }
                System.out.println("**************************************");
                System.out.println("Azimute: "+azimuteAnt);
                System.out.println("Dif. altura aparelhos: "+deltaAlturaAparelhos);
                System.out.println("Cotangente: "+cotangente);
                System.out.println("coordenada X (anterior) --  "+X);
                System.out.println("coordenada Y (anterior) --  "+Y);
                System.out.println("coordenada Z (anterior) --  "+Z);
                System.out.println("azimute anterior--"+azimutes[i-1]);
                X=X+distanciaAnt*Math.sin(Math.toRadians(azimuteAnt));
                Y=Y+distanciaAnt*Math.cos(Math.toRadians(azimuteAnt));
                Z=Z+deltaAlturaAparelhos+distanciaAnt*cotangente-fioMedioD;
                System.out.println("coordenada X (posterior) --  "+X);
                System.out.println("coordenada Y (posterior) --  "+Y);
                System.out.println("coordenada Z (posterior) --  "+Z);
                double anguloREP=anguloFormatoDecimal(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloRE());
                double anguloVANTEP=anguloFormatoDecimal(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getAnguloVANTE());
                double anguloP=anguloVANTEP-anguloREP;
                azimutes[i]=(azimutes[i-1]+anguloP-180);
                if(azimutes[i]>360){
                    azimutes[i]=azimutes[i]-360.0;
                }
                if(azimutes[i]<0.0){
                    azimutes[i]=azimutes[i]+360.0;
                }
            }
            DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDistanciaX(X);
            DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDistanciaY(Y);
            DAOTopoSingleton.getINSTANCE().getTopos().get(i).setDistanciaZ(Z);
   /*         System.out.println("coordenada X--"+X);
            System.out.println("coordenada Y--"+Y);
            System.out.println("coordenada Z--"+Z);
            System.out.println("coordenada X--"+DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX());
            System.out.println("coordenada Y--"+DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY());
            System.out.println("coordenada Z--"+DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaZ());*/
        }
    }

    public Bitmap poligonal(){
        Bitmap bitmap=Bitmap.createBitmap(318,461,Bitmap.Config.ARGB_8888);
        bitmap=bitmap.copy(bitmap.getConfig(), true);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(4f);
        paint.setTextSize(14f);
 /*       ArrayList<Coordenadas> coord = new ArrayList<Coordenadas>();
        Coordenadas coordenadas = new Coordenadas();
        coordenadas.setX(224.19);
        coordenadas.setY(589.25);
        coord.add(coordenadas);

        Coordenadas coordenadas1 = new Coordenadas();
        coordenadas1.setX(320.05);
        coordenadas1.setY(560.22);
        coord.add(coordenadas1);

        Coordenadas coordenadas2 = new Coordenadas();
        coordenadas2.setX(332.82);
        coordenadas2.setY(445.17);
        coord.add(coordenadas2);

        Coordenadas coordenadas3 = new Coordenadas();
        coordenadas3.setX(220.03);
        coordenadas3.setY(415.32);
        coord.add(coordenadas3);

        Coordenadas coordenadas4 = new Coordenadas();
        coordenadas4.setX(246.67);
        coordenadas4.setY(503.04);
        coord.add(coordenadas4);

        Coordenadas coordenadas5 = new Coordenadas();
        coordenadas5.setX(224.19);
        coordenadas5.setY(589.25);
        coord.add(coordenadas5); */

        double fatorescala;
        double comprimentoPadrao;
        double xmax;
        double ymax;
        double xmin;
        double ymin;
        double xMedio;
        double yMedio;
        double xMedia;
        double yMedia;
        double comprimentoX;
        double comprimentoY;
        comprimentoX=canvas.getWidth();
        comprimentoY=canvas.getHeight();
        if(comprimentoX>=comprimentoY){
            comprimentoPadrao=comprimentoY;
        }else{
            comprimentoPadrao=comprimentoX;
        }
        xmax=0.0;
        ymax=0.0;
        int n =DAOTopoSingleton.getINSTANCE().getTopos().size();
        for(int i=0;i<n;i++){
            if(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX()>xmax){
                xmax=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX();
            }
            if(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY()>ymax){
                ymax=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY();
            }
        }
        xmin=xmax;
        ymin=ymax;
        for(int i=0;i<n;i++){
            System.out.println("coordenada X(): "+DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX());
            System.out.println("coordenada Y(): "+DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY());
            if(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX()<xmin){
                xmin=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX();
            }
            if(DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY()<ymin){
                ymin=DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY();
            }
        }
        xMedio=(xmax-xmin)/2.0;
        yMedio=(ymax-ymin)/2.0;
        xMedia=(xmax+xmin)/2.0;
        yMedia=(ymax+ymin)/2.0;
        if(xMedio>=yMedio){
            fatorescala=(comprimentoPadrao/2.0)/xMedio;
        }else{
            fatorescala=(comprimentoPadrao/2.0)/yMedio;
        }
        int xPartida=(int)comprimentoX/2;
        int yPartida=(int)comprimentoY/2;
        for(int i=1;i<n;i++){
            float xInicial=(float)xPartida+((float)DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaX()-(float)xMedia)*(float)fatorescala;
            float yInicial=(float)yPartida-((float)DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaY()-(float)yMedia)*(float)fatorescala;
            System.out.println("valor de n e de i  "+n+"  "+i);
            float xFinal=(float)xPartida+((float)DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaX()-(float)xMedia)*(float)fatorescala;
            float yFinal=(float)yPartida-((float)DAOTopoSingleton.getINSTANCE().getTopos().get(i).getDistanciaY()-(float)yMedia)*(float)fatorescala;

                canvas.drawLine(xInicial,yInicial,xFinal,yFinal,paint);
                System.out.println("xinicial yinicial xfinal yfinal  "+xInicial+"  "+yInicial+" "+xFinal+" "+yFinal);



/*
            if(i<n){
                String ponto=Integer.toString(i);
                //           canvas.drawText(ponto, xInicial+5,yInicial-20,paint);
                ponto ="("+Double.toString(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaX())+","
                        +Double.toString(DAOTopoSingleton.getINSTANCE().getTopos().get(i-1).getDistanciaY())+")";
                canvas.drawText(ponto, xInicial+5,yInicial-5,paint);
            }
*/
        }
        return bitmap;
    }

}
