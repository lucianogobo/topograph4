package br.edu.iftm.lucianogobo.topograph3.data;

import java.util.ArrayList;

import br.edu.iftm.lucianogobo.topograph3.model.Topo;

public class DAOTopoSingleton {

    private static DAOTopoSingleton INSTANCE;

    //-----recurso globalmente acessado
    private ArrayList<Topo> topos;


    private DAOTopoSingleton(){
        this.topos =new ArrayList<>();
    }

    //Garantir que INSTANCE é modificado apenas 1 vez
    public static DAOTopoSingleton getINSTANCE(){
        if(INSTANCE==null){
            INSTANCE =new DAOTopoSingleton();
        }
        return INSTANCE;
    }
    //Definir regras aos recursos globais

    public ArrayList<Topo> getTopos(){
        return this.topos;
    }

    public void addStore(Topo topo){
        this.topos.add(topo);
    }

    public void modStore(Topo topo){
        int pos = this.topos.indexOf(topo);
        Topo topomod = this.topos.get(pos);
        topomod.setPonto(topo.getPonto());
        topomod.setDirecao(topo.getDirecao());
        topomod.setAnguloRE(topo.getAnguloRE());
        topomod.setAnguloVANTE(topo.getAnguloVANTE());
        topomod.setZenite(topo.getZenite());
        topomod.setFioInferior(topo.getFioInferior());
        topomod.setFioMedio(topo.getFioMedio());
        topomod.setFioSuperior(topo.getFioSuperior());
        topomod.setAlturaAparelho(topo.getAlturaAparelho());
    }

    public void removeStore(Topo topo) {
        System.out.println(topos.size());
        int pos = this.topos.indexOf(topo);
        this.topos.remove(pos);
        System.out.println(topos.size());
    }

    public int sendSize(){return this.topos.size();}

}
