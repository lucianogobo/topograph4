package br.edu.iftm.lucianogobo.topograph3.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import br.edu.iftm.lucianogobo.topograph3.R;

public class InputInitialActivities extends AppCompatActivity {


    public static final String RESULT_KEY="NewInitialTopoActivity.RESULT_KEY";
    static int numberOfVertices;
    static String azimuthFirst;
    public static String azimuthSecond;
    private TextView editNumberOfVertices;
    private TextView editAzimuthFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_initial_activities);
        this.editNumberOfVertices = findViewById(R.id.editTextNumber);
        this.editAzimuthFirst = findViewById(R.id.editTextTextPersonName);
    }

    public void onInsert(View view){
        String numeroVertices = this.editNumberOfVertices.getText().toString();
        numberOfVertices= Integer.parseInt(numeroVertices);
        String azimuteInicial;

        if(formatoAngulo(this.editAzimuthFirst.getText().toString())){
            azimuteInicial = this.editAzimuthFirst.getText().toString();
            azimuthFirst=azimuteInicial;
            azimuthSecond=azimuteInicial;
        }else{
            editAzimuthFirst.setText("");
            editAzimuthFirst.requestFocus();
            return;
        }

        if(numeroVertices.isEmpty() || azimuteInicial.isEmpty()){
            return;
        }else{
            Intent openNewStoreActivity = new Intent(this, NewImputTopoActivity.class);
            startActivity(openNewStoreActivity);
        }

        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    public void showAnguloInfo(String validador) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Erro na entrada de dados");
        dialogBuilder.setCancelable(false);
        dialogBuilder.setMessage(validador)
                .setPositiveButton(R.string.btnOK, (dialog, id) -> {
                    // FIRE ZE MISSILES!
                    dialog.cancel();
                    //        Toast.makeText(NewImputTopoActivity.this, R.string.btnOK + id, Toast.LENGTH_SHORT).show();
                });
        /*        .setNegativeButton(R.string.btnCancel, (dialog, id) -> {
                      dialog.cancel();
          //          Toast.makeText(NewImputTopoActivity.this, R.string.btnCancel + id, Toast.LENGTH_SHORT).show();
                });

  /*      dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        }); */
        dialogBuilder.create();
        dialogBuilder.show();
    }

    public boolean formatoAngulo(String angulo){
        if(!angulo.substring(0,4).matches("[0-9]*")){
            if(angulo.substring(0,3).matches("[0-9]*") ){
                int angulonumero=Integer.parseInt(angulo.substring(0,3));
                if(angulonumero<=360 && angulonumero>=0){
                    if(angulo.substring(3,4).matches("[.]")){
                        if(angulo.substring(4,6).matches("[0-9]*")){
                            if(angulo.substring(4,5).matches("[0-5]*")){
                                if(angulo.substring(6,7).matches("[']")){
                                    if(angulo.substring(7,9).matches("[0-9]*")){
                                        if(angulo.substring(7,8).matches("[0-5]*")){
                                            if(angulo.substring(9).matches("[\"]")){
                                                return true;
                                            }else{
                                                showAnguloInfo("Atrás dos segundos deve ter o simbolo  \" ");
                                                return false;
                                            }
                                        }else{
                                            showAnguloInfo("A dezena dos segundos deve variar entre zero e cinco");
                                            return false;
                                        }
                                    }else{
                                        showAnguloInfo("Os segundos devem ser numeros naturais");
                                        return false;
                                    }
                                }else{
                                    showAnguloInfo("Depois dos minutos, deve vir o simbolo ' ");
                                    return false;
                                }
                            }else{
                                showAnguloInfo("A dezena dos minutos só pode variar entre 0 e 5");
                                return false;
                            }
                        }else{
                            showAnguloInfo("Não pode haver letras nos minutos");
                            return false;
                        }

                    }else{
                        showAnguloInfo("Depois do angulo, deve vir o símbolo °");
                        return false;
                    }
                }else{
                    showAnguloInfo("O angulo não pode ser maior que 360 nem menor que zero");
                    return false;
                }
            }else{
                if(angulo.substring(0,2).matches("[0-9]*")){
                    //      System.out.println(angulo.substring(2,3));
                    if(angulo.substring(2,3).matches("[.]")){
                        if(angulo.substring(3,5).matches("[0-9]*")){
                            if(angulo.substring(3,4).matches("[0-5]*")){
                                if(angulo.substring(5,6).matches("[']")){
                                    if(angulo.substring(6,8).matches("[0-9]*")){
                                        if(angulo.substring(6,7).matches("[0-5]*")){
                                            if(angulo.substring(8).matches("[\"]")){
                                                System.out.println("O angulo é valido!");
                                                return true;
                                            }else{
                                                showAnguloInfo("Atrás dos segundos deve ter o simbolo \" ");
                                                return false;
                                            }
                                        }else{
                                            showAnguloInfo("A dezena dos segundos deve variar entre 0 e 5");
                                            return false;
                                        }
                                    }else{
                                        showAnguloInfo("Os segundos devem ser numeros naturais");
                                        return false;
                                    }
                                }else{
                                    showAnguloInfo("Depois do minuto, deve haver o símbolo '");
                                    return false;
                                }
                            }else{
                                showAnguloInfo("A dezena dos minutos só pode variar entre 0 e 5");
                                return false;
                            }
                        }else{
                            showAnguloInfo("Os minutos devem ser numeros naturais");
                            return false;
                        }
                    }else{
                        showAnguloInfo("Depois do angulo, deve vir o símbolo °");
                        return false;
                    }
                }else{
                    if(angulo.substring(0,1).matches("[0-9]*")){
                        System.out.println(angulo.substring(1,2));
                        if(angulo.substring(1,2).matches("[.]")){
                            if(angulo.substring(2,4).matches("[0-9]*")){
                                if(angulo.substring(2,3).matches("[0-5]*")){
                                    if(angulo.substring(4,5).matches("[']")){
                                        if(angulo.substring(5,7).matches("[0-9]*")){
                                            if(angulo.substring(5,6).matches("[0-5]*")){
                                                if(angulo.substring(7).matches("[\"]")){
                                                    System.out.println("O angulo e valido!");
                                                    return true;
                                                }else{
                                                    showAnguloInfo("Atrás dos segundos deve ter o simbolo \" ");
                                                    return false;
                                                }
                                            }else{
                                                showAnguloInfo("A dezena dos segundos deve variar entre 0 e 5");
                                                return false;
                                            }
                                        }else{
                                            showAnguloInfo("Os segundos devem ser numeros naturais");
                                            return false;
                                        }
                                    }else{
                                        showAnguloInfo("Depois do minuto, deve haver o símbolo '");
                                        return false;
                                    }
                                }else{
                                    showAnguloInfo("A dezena dos minutos só pode variar entre 0 e 5");
                                    return false;
                                }
                            }else{
                                showAnguloInfo("Os minutos devem ser numeros naturais");
                                return false;
                            }
                        }else{
                            showAnguloInfo("Depois do angulo, deve vir o símbolo °");
                            return false;
                        }
                    }else{
                        showAnguloInfo("O angulo deve ser um numero natural");
                        return false;
                    }
                }
            }
        }else{
            showAnguloInfo("O angulo não pode ser maior que tres digitos");
            return false;
        }
    }

}