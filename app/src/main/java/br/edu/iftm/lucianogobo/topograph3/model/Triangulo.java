package br.edu.iftm.lucianogobo.topograph3.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Triangulo implements Parcelable {

    private Coordenadas vertice1;
    private Coordenadas vertice2;
    private Coordenadas vertice3;
    private Coordenadas verticeMenor;
    private Coordenadas verticeMeio;
    private Coordenadas verticeMaior;

    public Triangulo(Coordenadas vertice1,Coordenadas vertice2,Coordenadas vertice3,
                     Coordenadas verticeMenor,Coordenadas verticeMeio,Coordenadas
                     verticeMaior){
        this.vertice1=vertice1;
        this.vertice2=vertice2;
        this.vertice3=vertice3;
        this.verticeMenor=verticeMenor;
        this.verticeMeio=verticeMeio;
        this.verticeMaior=verticeMaior;
    }

    public Triangulo(Coordenadas vertice1,Coordenadas vertice2,Coordenadas vertice3
                     ){
        this.vertice1=vertice1;
        this.vertice2=vertice2;
        this.vertice3=vertice3;
        this.verticeMenor=verticeMenor;
        this.verticeMeio=verticeMeio;
        this.verticeMaior=verticeMaior;
    }

    public Triangulo(
    ){

    }





    public Coordenadas getVerticeMenor() {
        return verticeMenor;
    }

    public void setVerticeMenor(Coordenadas verticeMenor) {
        this.verticeMenor = verticeMenor;
    }

    public Coordenadas getVerticeMeio() {
        return verticeMeio;
    }

    public void setVerticeMeio(Coordenadas verticeMeio) {
        this.verticeMeio = verticeMeio;
    }

    public Coordenadas getVerticeMaior() {
        return verticeMaior;
    }

    public void setVerticeMaior(Coordenadas verticeMaior) {
        this.verticeMaior = verticeMaior;
    }

    public Coordenadas getVertice1() {
        return vertice1;
    }

    public void setVertice1(Coordenadas vertice1) {
        this.vertice1 = vertice1;
    }

    public Coordenadas getVertice2() {
        return vertice2;
    }

    public void setVertice2(Coordenadas vertice2) {
        this.vertice2 = vertice2;
    }

    public Coordenadas getVertice3() {
        return vertice3;
    }

    public void setVertice3(Coordenadas vertice3) {
        this.vertice3 = vertice3;
    }

    public Coordenadas pontoCurvaNivel(Triangulo triangulo, int indice,double cota) {
        double distanciaH;
        double distanciaHC;
        double cosH;
        double senH;
        double deltaH;
        double deltaHC;
        double XI;
        double XF;
        double YI;
        double YF;
        double ZI;
        double ZF;
        if (indice == 1) {
            XF = triangulo.getVerticeMeio().getX();
            XI = triangulo.getVerticeMenor().getX();
            YF = triangulo.getVerticeMeio().getY();
            YI = triangulo.getVerticeMenor().getY();
            ZF = triangulo.getVerticeMeio().getZ();
            ZI = triangulo.getVerticeMenor().getZ();

        } else {
            if (indice == 2) {
                XF = triangulo.getVerticeMaior().getX();
                XI = triangulo.getVerticeMeio().getX();
                YF = triangulo.getVerticeMaior().getY();
                YI = triangulo.getVerticeMeio().getY();
                ZF = triangulo.getVerticeMaior().getZ();
                ZI = triangulo.getVerticeMeio().getZ();

            } else {
                XF = triangulo.getVerticeMaior().getX();
                XI = triangulo.getVerticeMenor().getX();
                YF = triangulo.getVerticeMaior().getY();
                YI = triangulo.getVerticeMenor().getY();
                ZF = triangulo.getVerticeMaior().getZ();
                ZI = triangulo.getVerticeMenor().getZ();
            }
        }
        distanciaH = Math.sqrt(Math.pow(XF - XI, 2) + Math.pow(YF - YI, 2));
        cosH = (XF - XI) / distanciaH;
        senH = (YF - YI) / distanciaH;
        deltaH = ZF - ZI;
        deltaHC=cota-ZI;
        distanciaHC=(distanciaH/deltaH)*deltaHC;
        Coordenadas ponto=new Coordenadas();
        ponto.setX(XI+cosH*distanciaHC);
        ponto.setY(YI+senH*distanciaHC);
        ponto.setZ(cota);
        return ponto;
    }

  // Parcelable--------------------------------------------------------------

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    protected Triangulo(Parcel in) {
    }

    public static final Creator<Triangulo> CREATOR = new Creator<Triangulo>() {
        @Override
        public Triangulo createFromParcel(Parcel in) {
            return new Triangulo(in);
        }

        @Override
        public Triangulo[] newArray(int size) {
            return new Triangulo[size];
        }
    };


}
